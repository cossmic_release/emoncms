<?php
global $path, $mysqli, $session;
$userid = $session['userid'];
$decomposedPath = explode("/", "$path");
$vdPath = "";
foreach($decomposedPath as &$value) {
    if((strcmp($value, "http:") == 0) || (strcmp($value, "https:") == 0)){
		$vdPath .= $value . "//";
    }else{
		if((empty($value) == false)  && (strcmp($value, "emoncms") !== 0)){
			$vdPath .= $value . "/";
		}
    }
}
/*
Pending
-- change ajax urls to proper address
--  userid is hardcoded in regard to virtual devices
*/
?>

<!-- Stylesheets -->
<link rel="stylesheet" type="text/css" href="<?php echo $path; ?>Lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $path; ?>Modules/cossmiccontrol/Views/cossmiccontrol_view.css">
<link rel="stylesheet" type="text/css" href="<?php echo $path; ?>Lib/pure-0.5.0/pure-min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $path; ?>Modules/cossmiccontrol/Views/css/scheduler.css">
<link rel="stylesheet" type="text/css" href="<?php echo $path; ?>Lib/jquerypowertip/css/jquery.powertip.css">


<!-- Javascript imports -->
<script type="text/javascript" src="<?php echo $path; ?>Lib/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="<?php echo $path; ?>Lib/jqueryui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $path; ?>Lib/bootstrap-datetimepicker-0.0.11/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo $path; ?>Lib/jquerypowertip/jquery.powertip.min.js"></script>
<script type="text/javascript" src="<?php echo $path; ?>Modules/cossmiccontrol/Views/json.js"></script>

<div id="settings">
	<!-- Row for the appliances and the configure containers -->
	<div class="row">

		<!-- Container with the available configurable devices -->
		<div id="cdpanel" class="panel span6 double">
			<div class="panel-heading">Available Appliances</div>
			<ul id="myConfigDeviceList">
			</ul>
		</div>
		
		<!-- Container for the configure single run device forms -->
		<div id="settingMidPane" class="panel span6 double">


     	<div class="panel-heading">Plan your appliance<img class = "helpIcon" id = "planHelp" src = "<?php echo $path; ?>images/help-icon.png"/></div>
     
			<div id="configDeviceForm">

				<form id="configureForm" class="form-inline">
					<label>Appliance Name: </label>  <span id="deviceNameOnConfigPane"></span><br> 
					<label>Earliest Start Time: </label> 
					<div id="estConfigDatetimepicker" class="input-append inputs">
						<input data-format="hh:mm" type="text" id="estInput">
						<span class="add-on">
							<i data-time-icon="icon-time" data-date-icon="icon-calendar">
							</i>
						</span>
					</div><div id="estPlusOne" style="display: inline"> +1 </div> <br />
					<label>Latest Start Time: </label>
					<div id="lstConfigDatetimepicker" class="input-append inputs">
						<input data-format="hh:mm" type="text" id="lstInput">
						<span class="add-on">
							<i data-time-icon="icon-time" data-date-icon="icon-calendar">
							</i>
						</span>
					</div><div id="lstPlusOne" style="display: inline"> +1 </div>
					<br />
          <div> <label>Maximum Delay to start: </label>  <span id="maxDelayOnConfig"></span> (hours:minutes)<br> </div>
          <div id="configModeDiv"><label id="configModeLabel">Program: </label> 
					<select id="selectModeConfigList" class="inputs">
						<option>1</option>
						<option>2</option>
					</select> </div>
					<button class="btn" id="addTaskeButton"  onclick="clickAddTask(event)" type="button">Add Task</button>      <button class="btn" id="editDefaultButton" onclick="" type="button">Edit Default Configuration</button>  
 <button id="forceSwitchButton" class="btn" onclick="">Force Switch On</button>
				</form>
			</div>


  
  		<!-- Container for the configure ecar -->
	

			<div id="configEVForm">
				<form id="configureEVForm" class="form-inline">
					<label>Appliance Name: </label>  <span id="evNameOnConfigPane"></span><br> 
					<div id="targetChargingLevelDiv" ><label>Target Charging Level (in %): </label><input class="config-input" type="text" value="" id="targetChargingLevel"></div>
          <label>Deadline to reach target level: </label>
					<div id="deadlineConfigDatetimepicker" class="input-append inputs">
						<input data-format="hh:mm" type="text" id="deadlineCondifInput">
						<span class="add-on">
							<i data-time-icon="icon-time" data-date-icon="icon-calendar">
							</i>
						</span></div> <br />
					<button class="btn" id="addEVTaskButton"  onclick="clickAddEVTask(event)" type="button">Add Task</button>      <button class="btn" id="editEVDefaultButton" onclick="" type="button">Edit Default Configuration</button>  
				</form>
			</div>
		</div>
	</div>
  
	
	<p />
	
	<!-- Row for the scheduled task list -->
	<div class="row">
		<div id="taskTablePanel" class="panel span12">
			<div class="panel-heading">Your scheduled tasks</div>
			<table class="table table-condensed bcolor" id="taskTable">
				<thead>
					<tr>
						<th>Appliance Name</th>
						<th>Status</th>
						<th>Program</th>
						<th>Earliest Start Time</th>
						<th>Latest Start Time</th>
						<th>Actual Start Time</th>
						<th>Actual Finishing Time</th>
						<th></th>
					</tr>
			  </thead>
			  <tbody>
			  </tbody>
			</table>
		</div>
	</div>
</div>

<div id="configSingleRunDefaultModal" class="modal hide fade" data-devid="">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Edit Default Device Configuration</h3>
  </div>                                                                                                                                                           
  <div class="modal-body">                                                                                                                                        
    <div id="estLstConfigBox">
       <input type="radio" name="defaultTypeRadios" id="estLstConfigRadio" value="estLstConfigMode"> Configuration in terms of fixed EST/LST  <img class = "helpIcon" id = "estLSThelpIcon" src = "<?php echo $path; ?>images/help-icon.png"/>
       	<form id="defaultESTLSTForm" class="form-inline">
					<label>Earliest Start Time: </label> 
					<div id="estDefaultDatetimepicker" class="input-append inputs" >
						<input data-format="hh:mm" type="text" id="estDefaultInput">
						<span class="add-on">
							<i data-time-icon="icon-time" data-date-icon="icon-calendar">
							</i>       
						</span>
					</div><br />
					<label>Latest Start Time: </label>
					<div id="lstDefaultDatetimepicker" class="input-append inputs" >
						<input data-format="hh:mm" type="text" id="lstDefaultInput">
						<span class="add-on">
							<i data-time-icon="icon-time" data-date-icon="icon-calendar">
							</i>
						</span>
					</div><br />
        </form>
                 
    </div> <hr>
    <div id="maxDelayConfigBox">
       <input type="radio" name="defaultTypeRadios" id="maxDelayConfigRadio" value="maxDelayConfigMode"> Configuration in terms of fixed maximum delay  <img class = "helpIcon" id = "maxDelayHelpIcon" src = "<?php echo $path; ?>images/help-icon.png"/>
       <form id="defaultMaxDelayForm" class="form-inline">
            <div id="defaultMaxDelayDiv" ><label>Max Delay: </label><input class="config-input" type="text" value="" id="defaultMaxDelay" data-dbid=""></div>
       </form>
    </div>  <hr>
          <div id="defaultModeDiv"><label id="defaultModeLabel" dbid="">Program: </label> 
					<select id="selectModeDefaultList" class="inputs">
						<option>1</option>
						<option>2</option>
					</select> </div>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">Close</a>
    <a href="#" class="btn btn-primary" onclick="saveDefaultConfgSingleRun()">Save changes</a>
  </div>
</div>

<div id="configEVDefaultModal" class="modal hide fade" data-devid="">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Edit E-car Default Configuration</h3>
  </div>                                                                                                                                                           
  <div class="modal-body">                                                                                                                                        
       	<form id="defaultEVConfigForm" class="form-inline">
					<label>Target Charging Deadline: </label> 
					<div id="evDefaultDatetimepicker" class="input-append inputs" >
						<input data-format="hh:mm" type="text" id="evDefaultInput" data-dbid="">
						<span class="add-on">
							<i data-time-icon="icon-time" data-date-icon="icon-calendar">
							</i>       
						</span>
					</div><br />
        <div id="defaultChargingLevelDiv" ><label>Target Charging Level (in %): </label><input class="config-input" type="text" value="" id="defaultChargingLevel" data-dbid=""></div>
         <div id="defaultBaseChargingLevelDiv"><label> Base Charging Level (to be reached ASAP, in %): </label><input class="config-input" type="text" value="" id="defaultBaseChargingLevel" data-dbid=""></div>	
        </form>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">Close</a>
    <a href="#" class="btn btn-primary" onclick="saveDefaultConfgEV()">Save changes</a>
  </div>
</div>


<script>

  $("#configEVForm").hide();
  $("#configDeviceForm").hide();
  $("#estPlusOne").hide();
  $("#lstPlusOne").hide();

 $(document).ready( function () {
    initSettings()
	scheduledTaskSetup();
	highlightPageLink();
} );

//function to find and add style to the link for the current page
function highlightPageLink(){
	var a = document.getElementsByTagName("a");
    for(var i=0;i<a.length;i++){
        if(a[i].href.split("#")[0] == window.location.href.split("#")[0]){
            a[i].id = "currentLink";
        }
    }
}


function setModalRadio(){
   var val = $("input[name=defaultTypeRadios]:checked").val();
    if (val == 'estLstConfigMode') {
      enableESTconf();  
    } else {
      enableDelayconf();  
    }
}

function initSettings(){

    //setting single run config modal radio button
    $('input[name=defaultTypeRadios]:radio').change(setModalRadio);


	//Ajax call to populate the appliance list
    $.ajax({
        url: '<?php echo $path; ?>devices/device/list.json',
        type: 'get',
        dataType: "json",
        success: function(output) {
			     console.log(output);
  			   $.each(output, function(idx, item){
      				var id = item.id;
			       	var name = item.name;
				      var operatingType = item.operatingType;
			        var productType = item.productType;
				      if(operatingType == "single_run" || operatingType == "ev" )  {
          					var listItem = $('<li  dev-id="' + id + '" dev-name="' + name + '" dev-operatingType="' + operatingType 
          					+  '"  dev-productType="' + productType + '"><a href="#">'  + name +'</a></li>');
          					var link = listItem.find( "a" );
          					link.attr( "onclick", "clickMyDeviceListItem(event,\""+ operatingType +"\","+ id +")" ); 
          					$("#myConfigDeviceList").append(listItem);
				     }
  		   });
          },
        error: function(xhr, desc, err) {
			console.log(xhr);
			console.log("Details: " + desc + "\nError:" + err);
        }
	}); // end ajax call
  
  // setting up the tooltips
    $("#planHelp").data("powertip", function(){
      var tooltip =   "You can plan this device schedule for a different behavior<br>"+
                      "then the default one"
  
      return tooltip;
  });
      $("#estLSThelpIcon").data("powertip", function(){
      var tooltip =   "The values here will be the standard EST and LST times<br>"+
                      "for this device when you manually put it in operation"
  
      return tooltip;
  });
      $("#maxDelayHelpIcon").data("powertip", function(){
      var tooltip =   "This setting means that the device will be effectively started<br>"+
                      "within the period between the moment you manually put it in operation <br>" +
                      " and the maximum delay set here"
  
      return tooltip;
  });
  
  
   $("#planHelp").powerTip({
            placement: "se",
            mouseOnToPopup: true
        });
    $("#estLSThelpIcon").powerTip({
            placement: "se",
            mouseOnToPopup: true
        });       
   $("#maxDelayHelpIcon").powerTip({
            placement: "se",
            mouseOnToPopup: true
        });        
        
              
  // end of setting up the help tooltip
  
  // setting up duration spinner

  $('#deadlineConfigDatetimepicker').datetimepicker({pickDate: false, pickSeconds: false}); 
  $('#evDefaultDatetimepicker').datetimepicker({pickDate: false, pickSeconds: false});
  $('#estDefaultDatetimepicker').datetimepicker({pickDate: false, pickSeconds: false});
  $('#lstDefaultDatetimepicker').datetimepicker({pickDate: false, pickSeconds: false});

	$('#estConfigDatetimepicker').datetimepicker({pickDate: false, pickSeconds: false});
	$('#lstConfigDatetimepicker').datetimepicker({pickDate: false, pickSeconds: false});
	
	// change date code for EST picker for adding a +1
	$('#estConfigDatetimepicker').on('changeDate', function(e) {
	  
	  var time = new Date();
	  var eventTime = new Date(e.localDate);
	  console.log(eventTime);
	  var cursorHour = eventTime.getHours();
	  var cursorMin = eventTime.getMinutes();
	  if( (cursorHour< time.getHours()) || ((cursorHour ==time.getHours()) && cursorMin< time.getMinutes()) ){
		  $("#estPlusOne").show();
	  }
	  else{
		 $("#estPlusOne").hide(); 
	  }
	});
  // change date code for LST picker for adding a +1 and setting the maxDelay
	$('#lstConfigDatetimepicker').on('changeDate', function(e) {
	  
    //set plus one                                 
	  var time = new Date();
	  var eventTime = new Date(e.localDate);
	  console.log(eventTime);
	  var cursorHour = eventTime.getHours();
	  var cursorMin = eventTime.getMinutes();
	  if( (cursorHour< time.getHours()) || ((cursorHour ==time.getHours()) && cursorMin< time.getMinutes()) ){
		  $("#lstPlusOne").show();
	  }
	  else{
		 $("#lstPlusOne").hide(); 
	  }
    // set delay
    var timeDiff =  eventTime - time;
    if (timeDiff <0) timeDiff+= (60 * 60 * 24 * 1000); // if it is a "previous day", Ill add 24h
    var hh = Math.floor(timeDiff / 1000 / 60 / 60);
    timeDiff -= hh * 1000 * 60 * 60;
    var mm = Math.floor(timeDiff / 1000 / 60);
    var textForDelay = "";
    if(hh>0) textForDelay += hh +":";
    if(mm<10) textForDelay += "0";
    textForDelay += mm;
    $("#maxDelayOnConfig").text("" + textForDelay);
	});
}



function clickMyDeviceListItem(event,operatingType,deviceId){

     var target = $( event.target );
     $("#myConfigDeviceList").children().removeClass("pure-menu-selected");
     target.parent().addClass("pure-menu-selected");
     var selectedListItem =  target.parent();
     
     
     //if the device is a ev
     if(operatingType == "ev" )  {
          $("#evNameOnConfigPane").text(selectedListItem.attr("dev-name"));
         $("#configEVForm").show();
         $("#configDeviceForm").hide();
         $("#editEVDefaultButton").unbind('click').click ( function () {clickEditDefaultEV(deviceId);});
          }
     else
      if  (operatingType == "single_run") {
     //if the device is single run
          $("#deviceNameOnConfigPane").text(selectedListItem.attr("dev-name"));
          $("#selectModeConfigList").empty();
            $("#forceSwitchButton").unbind('click').click ( function () {switchOn(deviceId);});
  
          
          var getInfo = '?deviceid='+deviceId;
                $.ajax({
          url: '<?php echo $vdPath; ?>emoncms/devices/device/get.json'+getInfo,
          type: 'get',
          dataType: 'json',
          success: function(output) {
                  console.log(JSON.stringify(output));
                   $("#editDefaultButton").unbind('click').click ( function () {clickEditDefaultSingleRun(deviceId,output.templateid);});
                   var getInfo = '?templateid='+output.templateid;
                   $.ajax({
						    url: '<?php echo $vdPath; ?>emoncms/devices/template/get.json'+getInfo,
							type: 'get',
							dataType: 'json',
							success: function(output) {
                  
							$.each(output.modes, function(idx, item){
							var ar=item.split(":");
                    $('#selectModeConfigList').append($("<option></option>").attr("value",item).text(ar[1])); 
                  });
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log('Details: ' + desc + '\nError:' + err);
          }
          }); // inner end ajax call
      	}
         });
           $("#configEVForm").hide();
         $("#configDeviceForm").show();
         
     }
    
     
     //code common for both
    
    $("#maxDelayOnConfig").text("");
	$("#settingMidPane").show();
	$("#settingMidPane").css({"display": "inline-block"});
    
}

function clickEditDefaultEV(deviceId){

 	
          //Ajax call to get the device information
			$.ajax({
				url: '<?php echo $vdPath; ?>emoncms/devices/device/parameters.json',
				data: {'deviceid': deviceId, 'description': 'true'},
				devid : deviceId,
				type: 'get',
				success: function(output) {
					if(null == output || output.length == 0 ){
						return;
					}
					// else
          var minEngTgt; 
          var engTgt; 
          var deadline;
          for(var i=0; i < output.length; i++){

            if(output[i].name == "minimum_energy_target" && output[i].value){
                $("#defaultChargingLevel").val(output[i].value);
 
            }
           if(output[i].name == "energy_target" && output[i].value){
                $("#defaultBaseChargingLevel").val(output[i].value); 
            }
           if(output[i].name == "target_deadline" && output[i].value){
                var date = parseHHMMstringToDate(output[i].value);
                var picker = $('#evDefaultDatetimepicker').data('datetimepicker');
                picker.setLocalDate(date);
            }

          }        
							 }

			}); // end ajax call
       $("#configEVDefaultModal").attr("data-devid", deviceId);

    $('#configEVDefaultModal').modal('show');
}


function clickEditDefaultSingleRun(deviceId, templateId){


          $("#defaultMaxDelay").val("");

            // get modes
            $("#selectModeDefaultList").empty();
            var getModes = '?templateid='+templateId;
            var promise =   $.ajax({
						    url: '<?php echo $vdPath; ?>emoncms/devices/template/get.json'+getModes,
							type: 'get',
							dataType: 'json',
							success: function(output) {
               $("#selectModeDefaultList").empty();   
							$.each(output.modes, function(idx, item){
							var ar=item.split(":");
                    $('#selectModeDefaultList').append($("<option></option>").attr("value",item).text(ar[1])); 
                  });
          },
          error: function(xhr, desc, err) {
            console.log(xhr);
            console.log('Details: ' + desc + '\nError:' + err);
          }
          }); // inner end ajax call


 	
          //Ajax call to get the device information
			$.ajax({
				url: '<?php echo $vdPath; ?>emoncms/devices/device/parameters.json',
				data: {'deviceid': deviceId, 'description': 'true'},
        prevPromise : promise,
				devid : deviceId,
				type: 'get',
				success: function(output) {
					if(null == output || output.length == 0 ){
            enableESTconf();
						$("#estLstConfigRadio").prop("checked", true); // TODO: this should be replaced by an exception as we do not want someone to set a default EST if we do not have the parameter
						return;
					}
          
          // clear up fields
          var day = new Date(2016, 05, 05, 00, 00, 00, 00);
          var estpicker = $('#estDefaultDatetimepicker').data('datetimepicker');
          estpicker.setLocalDate(day);
          var lstpicker = $('#lstDefaultDatetimepicker').data('datetimepicker'); 
          lstpicker.setLocalDate(day);
 
          
          
          for(var i=0; i < output.length; i++){
            // ill enable the checkbox area based on solelty the default_max_delay
            if(output[i].name == "default_max_delay"){
               // $("#defaultMaxDelay").attr("data-dbid", output[i].id);
                if(output[i].value){
                  enableDelayconf();
                  $("#maxDelayConfigRadio").prop("checked", true);
                  $("#defaultMaxDelay").val(output[i].value);
                  //$("#estDefaultInput").val("00:00");
                }
                else{
                  enableESTconf();
                   $("#estLstConfigRadio").prop("checked", true);
                   
                } 
            }
            if(output[i].name == "default_EST" && output[i].value){
                //$("#estDefaultInput").attr("data-dbid", output[i].id);
                var date = parseHHMMstringToDate(output[i].value);
                //var estpicker = $('#estDefaultDatetimepicker').data('datetimepicker');
                estpicker.setLocalDate(date);
            }
            if(output[i].name == "default_LST" && output[i].value){
                //$("#lstDefaultInput").attr("data-dbid", output[i].id);
                var date = parseHHMMstringToDate(output[i].value);
                 //var lstpicker = $('#lstDefaultDatetimepicker').data('datetimepicker');
                lstpicker.setLocalDate(date);
            }
            if(output[i].name == "default_mode"){
                this.prevPromise.then( setSingleRunModalDefaultMode(output[i].id, output[i].value) ); 
            }
          } //end of  for(var i=0; i < output.length; i++){
          }

			}); // end ajax call
          
      $("#configSingleRunDefaultModal").attr("data-devid", deviceId);
    $('#configSingleRunDefaultModal').modal('show');
}


function saveDefaultConfgSingleRun(){
      // get values from modal
      var id = $("#configSingleRunDefaultModal").attr("data-devid"); 
      var selectedMode = $('#selectModeDefaultList').find(":selected").val();
     
      
      
      var selectedDelay =  $("#defaultMaxDelay").val();
       
      var lstpicker = $('#lstDefaultDatetimepicker').data('datetimepicker');
      var selectedLST =  lstpicker.getLocalDate();
      var estpicker = $('#estDefaultDatetimepicker').data('datetimepicker');
      var selectedEST =  estpicker.getLocalDate();
       
    	var fieldsText = '{"default_mode":"'+ selectedMode + '","default_max_delay":';
      
			if($('#maxDelayConfigRadio').is(':checked')) {// default delay is set 
         fieldsText += '"' + selectedDelay + '","default_EST":null,"default_LST":null}'; 
      }else{
         var estString =  "" + selectedEST.getHours() + ":" + selectedEST.getMinutes(); 
         var lstString =  "" + selectedLST.getHours() + ":" + selectedLST.getMinutes();
         fieldsText += 'null,"default_EST":"'+estString+ '","default_LST":"'+lstString+ '"}';
         
      }
			$.ajax({
				url: '<?php echo $vdPath; ?>emoncms/devices/device/setparameters.json',
				data: {
					'deviceid': id,
					'fields' : fieldsText
				},
				type: 'get',
				success: function(output) {
					alert("Device successfully configured");
					$('#configSingleRunDefaultModal').modal('hide');
				},
				error: function(xhr, desc, err) {
							console.log(xhr);
              alert("Failed to configure device");
					    $('#configSingleRunDefaultModal').modal('hide');
				}
			}); // end ajax call
}

function saveDefaultConfgEV(){
      var id = $("#configEVDefaultModal").attr("data-devid"); 
      var targetLevel =  $("#defaultChargingLevel").val();
      var baseLevel =  $("#defaultBaseChargingLevel").val();
       
      var picker = $('#evDefaultDatetimepicker').data('datetimepicker');
      var time =  picker.getLocalDate();
      var timeString =  "" + time.getHours() + ":" + time.getMinutes(); 

      var fieldsText = '{"minimum_energy_target":"'+ targetLevel + '","energy_target":"'+baseLevel+ '","target_deadline":"'+timeString+ '"}';

      $.ajax({
				url: '<?php echo $vdPath; ?>emoncms/devices/device/setparameters.json',
				data: {
					'deviceid': id,
					'fields' : fieldsText
				},
				type: 'get',
				success: function(output) {
					alert("EV successfully configured");
					$('#configEVDefaultModal').modal('hide');
				},
				error: function(xhr, desc, err) {
							console.log(xhr);
              alert("Failed to configure ev");
					    $('#configEVDefaultModal').modal('hide');
				}
			}); // end ajax call

}


// parses a HH:MM or HH:MM:SS string to a Date corresponding on today with the input hour and minutes
// seconds are ignored
function parseHHMMstringToDate(dateString){
      var dParts = dateString.split(":");
      var hours =  dParts[0];
      var minutes = dParts[1];
      var date = new Date();
      date.setHours(hours);
      date.setMinutes(minutes);
      return date;
}

function setSingleRunModalDefaultMode(modeId,modeValue){
    //$("#defaultModeLabel").attr("data-dbid", modeId);
    if(modeValue != null){
        $("defaultModeDiv select").val(modeValue);
    }
}

function enableESTconf(){


   
    $("#lstDefaultInput").prop('disabled', false);
    $("#estDefaultInput").prop('disabled', false);
    $("#defaultMaxDelay").prop('disabled', true);
}

function enableDelayconf(){
    
    $("#lstDefaultInput").prop('disabled', true);
    $("#estDefaultInput").prop('disabled', true);
    $("#defaultMaxDelay").prop('disabled', false);    
}

function clickAddEVTask(event){
}

function clickAddTask(event){
      // get selected device
      var selectedListItem = $("#myConfigDeviceList").children(".pure-menu-selected");
      var deviceId = selectedListItem.attr("dev-id");
      var estString = $('#estInput').val();
      var lstString = $('#lstInput').val();
      var lstArray =  estString.split(":");
      var mode= $('#selectModeConfigList').val();
	  var name = selectedListItem.attr("dev-name");
      var operating_type =  selectedListItem.attr("dev-operatingtype");
      
      if(null == mode) 
         mode = ["0","default"];
      else
		 mode =mode.split(":"); 
      if (mode.length<2)
         mode = [mode,mode]; 
      // check validity of input times
      var currentTime = new Date();
      var currHours = currentTime.getHours();
      var currMinutes = currentTime.getMinutes();
      if  (currHours <10)  currHours = "0" + currHours;// adjust currHours to hh format
      if  (currMinutes <10)  currMinutes = "0" + currMinutes;// adjust currMinutes to hh format
      var nowString =  currHours + ":"  +  currMinutes;

      // THIS COMPARRISSON ONLY WORK BECAUSE THE DATES ARE IN 24h format      
      if(nowString > estString){
        window.alert("cant have a start date earlier then current timestamp " + nowString);
        return;
      }else{
        if(estString >= lstString ) {
          window.alert("latest start time must be higher than earliest start time");
          return;
        }
      }
      // end of validity check
      
        //salvatore: I add current Year and Month (The schedule shoudl be possible for the next day)
      
      estString = currentTime.getFullYear()+"-"+(currentTime.getMonth()+1)+"-"+currentTime.getDate()+" "+estString;
      lstString = currentTime.getFullYear()+"-"+(currentTime.getMonth()+1)+"-"+currentTime.getDate()+" "+lstString;
      
      //TODO: mode and execution type are currently hard coded
      var addTaskJson = '?json={"EST":"' + estString+ '","LST":"' + lstString + '","deviceID":"'+ deviceId+'","execution_type":"'+ operating_type  +'","mode":"' +mode[0]+'","modeName":"' +mode[1]+'","appName":"'+name+'"}';
      console.log(addTaskJson);
      $.ajax({
        url: '<?php echo $path; ?>mas/add.json' + addTaskJson,
        type: 'get',
        dataType: 'html',
        success: function(output) {
                console.log(output);
                if(null != output) {
                  if (null != output.error ){
                     console.log(output.error);
                  }
                }
                location.reload();
        },
        error: function(xhr, desc, err) {
          console.log(xhr);
          console.log('Details: ' + desc + '\nError:' + err);
        }
  }); // end ajax call 
}

function scheduledTaskSetup(){
    //var listTaskJson = '?json={"status":10,"info":"true"}';
    var listTaskJson = '?json={"info":"true"}';
    $.when(
        $.ajax({
			url: '<?php echo $path; ?>mas/list.json' + listTaskJson,
			type: 'get',
			dataType: 'json'
		})
       ,
		$.ajax({
            url: '<?php echo $path; ?>devices/device/list.json',
            type: 'get',
            dataType: 'json'
        })
    )
    .then(function( resultListTask,resultListDevices) {
        console.log(JSON.stringify(resultListTask[0]));
        console.log(JSON.stringify(resultListDevices[0]));
        var deviceHash = new Object();
        $.each(resultListDevices[0], function(idx, item){
            deviceHash[item.id] = item.name;
        });
        $.each(resultListTask[0].tasks, function(idx, item){
            var id = item.id;
      			var devId = item.deviceID;
      			var status;
      			switch(item.status) {
					case "-1":
      					status = "Waiting the device"
      					break;
      				case "0":
      					status = "Not yet scheduled"
      					break;
      				case "1":
      					status = "Scheduled"
      					break;
      				case "4":
      					status = "Running"
      					break;
      				case "5":
      					status = "Completed"
      					break;
                 case "6":
      					status = "Completed"
      					break;
      				default:
      					status = "Undetermined"
      			}
      		   
      			var mode = item.mode;
      			if (item.modeName !=null)
      			   mode=item.modeName
      			var est = item.EST;
      			var lst = item.LST;
      			var aet = item.AET;
      			var ast = item.AST;
      			var name;
      			if (deviceHash.hasOwnProperty(devId)) {
      				console.log("has own property");
      				name = deviceHash[devId];
      			}
      			var htmlRow= '<tr task-id="' +id+ '" device-id="' + devId + '"><td>' + name + ' </td><td> ' + status + ' </td><td> ' + mode +  ' </td><td> ' + est + ' </td><td> ' + lst +
      			' </td><td> ' + ast + ' </td><td>' + aet + '</td><td><a href="#" onclick="deleteTask('+id+',\''+aet+'\',\''+item.status+'\')"><i class="icon-trash"></i></a></td></tr>';
      			$("#taskTable > tbody").prepend(htmlRow);        
        });
   });
}

function deleteTask(id,aet,status){
    
    /*
    salvatore commented out this code
	if (aet != "UNDEFINED"){
		window.alert("It is not possible to delete scheduled tasks ");
		return;
	}
   */
    if ((status=="4") || (status=="5") || (status=="6")){
        window.alert("It is not possible to delete scheduled tasks because running or completed!");
        return;
        }
    
   
	$.ajax({
        url: '<?php echo $path; ?>mas/delete.json?id=' + id,
        type: 'get',
        dataType: 'json',
        success: function(output) {
            console.log(output);
            if(null != output) {
                if (output.result != "success"){
                    if(!output.result){ // task deleted
                        window.alert("It was not possible to delete the task, it probably has already been scheduled");
                    }
                    location.reload();
                }
            }
             location.reload();
        },
        error: function(xhr, desc, err) {
			console.log(xhr);
			console.log('Details: ' + desc + '\nError:' + err);
			window.alert("error when trying to delete the task");
        }
	}); // end ajax call
}

// sfor switch on of smar plug
		function switchOn(id){
			//http://127.0.0.1:4567/emoncms/devices/device/setparameters.json?deviceid=2&fields={[{'name':'orientation', 'value':43}]}
	 
			
			$.ajax({
				url: '<?php echo $vdPath; ?>emoncms/devices/device/status.json',
				data: {
					'deviceid': id,
					'status' : 1
				},
				type: 'get',
				success: function(output) {
					alert("Device Switched On");
					location.reload();
				},
				error: function(xhr, desc, err) {
							console.log(xhr);
				}
			}); // end ajax call
		}
</script>
