#!/usr/bin/python
import urllib2
import re
import requests
import six.moves.configparser
import pymysql
from random import random

config = six.moves.configparser.RawConfigParser()
config.read('gamification.cfg')



#recreates the json array as a python array
def json_to_array(url, split_char):
    req = requests.get(url)

    req_text= str(req.text)

    req_text = req_text.split(split_char)
    req_text[0]=req_text[0].lstrip('{[')
    req_text[-1]=req_text[-1].rstrip(']}')
    new_req_text=[]
    for r in req_text:
        text_split = r.split(',')
        new_req_text.append(text_split)
    return new_req_text
def getapikey():
    #with open("/var/www/emoncms/settings.php", 'r') as settings:
    with open("/home/bent/cossmig/emoncms/default.settings.php", 'r') as settings:
        for line in settings:
            if "$server" in line:
                server = line[17:-3]
            elif "$database" in line:
                database = line[17:-3]
            elif "$username" in line:
                username = line[17:-3]
            elif "$password" in line:
                password = line[17:-3]
        print(server, database, username, password)
    '''db = pymysql.connect(host=server, user=username, passwd=password, db=database)
    cursor = db.cursor()
    cursor.cursor.execute("SELECT apikey_read FROM users;")
    apikey = str(cursor.fetchone())[2:-3]
    cursor.close()
    db.close()'''
    apikey = 'fa0b1949a204845142e4a70e4e9e2429'
    return apikey
addr = "10.0.0.20" #127.0.0.1
uid = "1"
apikey = getapikey()


def get_household_id():
	with open("/home/bent/cossmig/emoncms/default.settings.php") as settings:
		for line in settings:
			if "$cossmic['node']" in line:
				household_id = (re.sub(r'[^\d.]+','',line))
				return 14#household_id
	return - 1

def get_consumption():
	k = float(config.get('Multipliers', 'consumption'))
	feed = config.get('Feeds', 'consumption')
	print(feed)
	try:
		configonsumption = requests.get('http://'+addr+'/emoncms/feed/value.json?id='+str(feed)+'&apikey='+apikey)
		print(consumption.text)
		return k*float(consumption.text[1:-1])
	except:
		return -1

def get_production():
	k = float(config.get('Multipliers', 'production'))
	feed = config.get('Feeds', 'production')
	print(feed)
	try:
		production = requests.get('http://'+addr+'/emoncms/feed/value.json?id='+str(feed)+'&apikey='+apikey)
		print(production.text)
		return k*float(production.text[1:-1])
	except:
		return -1

def get_shared():
	k = float(config.get('Multipliers', 'shared'))
	shared = round(random()*7)
	return k*shared

def get_scheduled():
	k = float(config.get('Multipliers', 'scheduled'))
	scheduled = round(random()*7)
	return k*scheduled

def get_ip():
	with open("gamification-config.js",'r') as config:
		text = config.read()
		IP = str(re.findall(ur'"[^"]*"', text))[3:-3]
		print (IP)
	return IP

household_id = get_household_id()
consumption = get_consumption()
production = get_production()
shared = get_shared()
scheduled = get_scheduled()
ip = get_ip()

url = "http://"+str(ip)+"/php/updateHouseholdScores.php?household_id="+str(household_id)+"&consumption="+str(consumption)+"&production="+str(production)+"&scheduled="+str(scheduled)+"&shared="+str(shared)

print(url)
request = urllib2.Request(url)
print(urllib2.urlopen(request).read())
