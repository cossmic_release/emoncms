import scipy.interpolate as inter
import numpy as np
#import pylab as plt
import time
from numpy import linspace, poly1d
from numpy.lib.function_base import append
from StringIO import StringIO 
import os 
import sys 

path='/var/www/emoncms/profiles/'

if(len(sys.argv)!=2):
    sys.exit()

profname=sys.argv[1]
csv = np.genfromtxt (path+profname+'.raw', delimiter=",")

if len(csv) >= 2:
	
	time_data = csv[:,0]
	ydata = csv[:,1]


	isort = np.argsort(time_data)
	xx = np.arange(time_data[isort[0]],time_data[isort[len(isort)-1]],(time_data[isort[len(isort)-1]]-time_data[isort[0]])/len(time_data))


	#plt.plot(time_data[isort], ydata[isort],'b.', label="poly3")

	ncoffs = np.polyfit(time_data[isort], ydata[isort],3)
	#print ncoffs

	updated=np.poly1d(ncoffs)


	outfile = open(path+profname+'.prof', 'w')
	coffs=np.append( time_data[isort[len(isort)-1]],ncoffs)
	np.savetxt(outfile,coffs)
	outfile.close()

	outfile = open(path+profname+'.prof_raw', 'w')

	yy = updated(xx)
	for i in range(0,len(yy)):
		if yy[i]<0:
			yy[i]=0
		elif yy[i]<yy[i-1]:
		    yy[i]=yy[i-1]
			

	np.savetxt(outfile,np.column_stack((xx,yy)),fmt='%.0f %.4f')
	outfile.close()
#os.remove('/var/www/emoncms/profiles/'+profname+'.raw')

#plt.plot(xx,updated(xx),'r-', label="poly3")

#plt.show()

