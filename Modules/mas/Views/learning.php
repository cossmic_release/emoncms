<?php 
  global $path;
  /*$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => "http://localhost:8008/"
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// Close request to clear up some resources
		curl_close($curl);
		$resp=strtr ($resp, array ("'" => '"'));
	
   */
   
   $resp=Array();
   
?>

<script type="text/javascript" src="<?php echo $path; ?>Modules/mas/script/plotly-latest.min.js"></script>
<script type="text/javascript" src="<?php echo $path; ?>Lib/tablejs/table.js"></script>
<script type="text/javascript" src="<?php echo $path; ?>Lib/tablejs/custom-table-fields.js"></script>
<script type="text/javascript" src="<?php echo $path; ?>Modules/mas/Views/mas.js"></script>



<style>
input[type="text"] {
     width: 88%; 
}

#table td:nth-of-type(1) { width:5%;}
#table td:nth-of-type(2) { width:10%;}
#table td:nth-of-type(3) { width:25%;}

#table td:nth-of-type(7) { width:30px; text-align: center; }
#table td:nth-of-type(8) { width:30px; text-align: center; }
#table td:nth-of-type(9) { width:30px; text-align: center; }
</style>






<div id="apihelphead"><div style="float:right;"><a href="api"><?php echo _('MAS API Help'); ?></a></div></div>

<div class="container">
 <h2> Available Device Profiles</h2>
 <div id="table"></div> 
 <div id="pinfo"></div>
 <div id="modes"></div>
 <div id="ts">
	 </div>
	 
</div>

<script>

function automata(id)
{
   window.open ('automata.html?profile='+id,'_self',false) 
    
    }


function viewprofile(id)
{
 
   /*
	var profile = mas.profile(id);
	
	$('#pinfo').html('<table><tr><td><b>Profile Type</b></td><td>'+profile["profile_type"]+'</td></tr></table>');
	*/

   

	var tsprofile = mas.tsprofile(id);

    var modes=tsprofile["modes"];
    var select = '<select name="modes">';
    
    modes=modes.substring(1,modes.length-1);

    var modesarray=modes.split(",");
    for(i=0;i<modesarray.length;i++)
    {
        modesarray[i]=modesarray[i].substring(1,modesarray[i].length-1);
        var temp = modesarray[i].split(':');
        
        select+='<option value='+temp[0]+'>'+temp[1]+' </option>';
        
    }
    select+='</select>';
    $('#modes').html(select);
    tsprofile ['name'] ='profile';
	var data = [tsprofile];
	data =e2p(data);
    
    var profile  = mas.profileinfo(id);
    if(profile['operatingType'] == "continuously_run")
      {
            
          
          
            var to = Date.now();
            var d = new Date();
            d.setHours(0,0);
            var from = d.getTime();
            
            
            
            dayvalues = mas.feedaverage(profile['feedid'],from,to);
            
            var x = [];
            var y = [];
            for (i = 0; i < dayvalues.length; i++)
            { 
                
                x.push(dayvalues[i][0]);
                y.push(dayvalues[i][1]);
                }
                
         

            var dailyprofile = {'x':x,'y':y, 'name': 'today'};
          
             
            data.push(e2p([dailyprofile])[0]);
            /*
            for (i = 0; i < data[0].x.length; i++)  
                {
                    var temp = new Date(data[0].x[i]+to);
                    data[0].x[i]=temp.getHours()+":"+temp.getMinutes();
                }
            for (i = 0; i < data[1].x.length; i++)  
                {
                    var temp = new Date(data[1].x[i]+to);
                    data[1].x[i]=temp.getHours()+":"+temp.getMinutes();
                }*/
            
            
      }      
     
    
	Plotly.newPlot('ts', data);
} 

function e2p(data)
{
    
     
    
    
    var x = data[0].x;
    var y = data[0].y;
    
    var temp = 0;
    var e = [0];
   
    for (i = 1; i < x.length; i++) {
       temp = (y[i]-y[i-1])/(x[i]-x[i-1]);
       e.push(temp);
       
    }
   
    data[0].y = e;
    
    return data;
    }

</script>

<script>
	
	 

	
 var path = "<?php echo $path; ?>";
 
 
 
 
 
   // Extend table library field types
  for (z in customtablefields) table.fieldtypes[z] = customtablefields[z];

  table.element = "#table";

  table.fields = {
    'id':{'type':"fixed"},
    'deviceid':{'title':'<?php echo _("device"); ?>','type':"fixed"},
    'modes':{'title':'<?php echo _("modes"); ?>','type':"fixed"},
    //'type':{'title':'<?php echo _('type'); ?>','type':"fixed"},
	'status':{'title':'<?php echo _("status"); ?>','type':"fixed"},
	//Actions
	'view-profile':{'title':'<?php echo _("loads"); ?>', 'type':"iconjs", 'link':'viewprofile'},
    'view-action':{'title':'automata', 'type':"iconjs", 'link':'automata'}
  }
  
  
  
  //table.groupprefix = "Driver ";
  //table.groupby = 'id';

  update();

  function update()
  {
    table.data = mas.profiles();
    table.draw();
  }
 
 
 
 
</script>
