<?php

/*
     All Emoncms code is released under the GNU Affero General Public License.
     See COPYRIGHT.txt and LICENSE.txt.

     ---------------------------------------------------------------------
     Emoncms - open source energy visualisation
     Part of the OpenEnergyMonitor project:
     http://openenergymonitor.org

*/

// no direct access
defined('EMONCMS_EXEC') or die('Restricted access');


class MASInterface
{
    private $mysqli;
    private $redis;
    private $log;
    private $loadsdir="/home/www-data/Dropbox/cossmicprototype/neighbour";
    private $devices;
    private $feed;
    
    public function __construct($mysqli,$redis, $feed,$input)
    {
        $this->mysqli = $mysqli;
        $this->redis = $redis;
        $this->log = new EmonLogger(__FILE__);
        $this->feed = $feed;
		$this->input = $input;
		 
    }
	
	 public function set_process($process)
    {
		 $this->process = $process;
		 
    }
	
	
	
    
    public function set_devices($devices)
    {
		$this->devices=$devices;
		} 
     
	 
	 
     public function predictions(){
         $files= array_map('basename', glob("/var/cossmic/taskscheduler/*.csv"));
         $table= Array();
         foreach ($files as $id)
           $table[]=array("id"=>$id);
           
        return $table;
        
	}
     
	 public function profiles(){
		$profiles = Array();
		/*
       $dir = "/var/www/emoncms/profiles";
		$dh  = opendir($dir);
		
		
     
        $ids = Array();
        while (false !== ($filename = readdir($dh))) 
		{
			if(!is_dir($dir."/".$filename))
			{			
			  
				$profile= Array();
				$profile["id"]=$filename;
				$profile["user"]=substr($filename,1, strpos($filename,"d")-strpos($filename,"u")-1);
				$profile["deviceid"]=substr($filename,strpos($filename,"d")+1,strpos($filename,"m")-strpos($filename,"d")-1);
				$profile["modeid"]=substr($filename,strpos($filename,"m")+1,strpos($filename,".")-strpos($filename,"m")-1);
				$profile["filename"]= $filename;
				$profile["source"]= "file";
				$profile["available"]= 1;
				$profiles[]=$profile;
				$ids[]=$filename;
		    }
			  		  
		  }*/
        $profileids = $this->redis->keys("profiles:*");
        foreach ($profileids as $id)
        {
			
			 $profile= Array();
			 $temp = $this->redis->hGetAll($id);
		  /*if(!in_array($temp["filename"],$ids)){*/		
			 $profile["id"]=$id;		 	 
			 $profile["userid"]=$temp["userid"];
			 $profile["deviceid"]=$temp["deviceid"];
			 $profile["modeid"]=$temp["mode"];
			 $profile["filename"]= $temp["filename"];	
			 $profile["status"]= $temp["status"];
             
            $profile["modes"]=$temp["modes"];
               
			 $profiles[]=$profile;	
		   /*}*/
		}
		
		
		
		
		return $profiles;		
		}
		
   public function getTSPrediction($filename){
       
       
       
		$ts = Array();
		$axis1 = Array();
		$axis2 = Array();
		$filename = "/var/cossmic/taskscheduler/".$filename;
		if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

      
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, " ")) !== FALSE)
            {
                
                    $axis1[] =  $row[0];
                    $axis2[] =  $row[1];
            }
            fclose($handle);
        }
       $ts["x"]=$axis1;
		$ts["y"]=$axis2;
		$ts["type"]="scatter";
        return $ts;

   }     
    


    public function getRAWProfile($profileid){
        
        $profile = $this->redis->hgetall($profileid);
        // var_dump($profile);
     
       $filename = "/var/www/emoncms/profiles/".$profile["filename"]."_raw";
     
       if(file_exists( $filename )) 
       {
         $ts = Array();
                $axis1 = Array();
                $axis2 = Array();
                
                $myfile = fopen($filename, "r") or die("Unable to open file!");
             
            
                
                while(! feof($myfile))
                {
                    
                    $sample=fgets($myfile);
                    $values = explode(" ", $sample);
                    if (count($values)==2){
                        $axis1[]=floatval($values[0])/1000;
                        $axis2[]=floatval($values[1]);
                    
                    }
                    }
                fclose($myfile);
                $ts["x"]=$axis1;
                $ts["y"]=$axis2;
                
                
       }
       else{
           $ts["x"]=array(0);
            $ts["y"]=array(0);
           }
                $ts["type"]="scatter";
                $ts["modes"]=$profile["modes"];
                return $ts;
        
        
    }
    
	public function getTSProfile($profileid){
        
        $profile = $this->redis->hgetall($profileid);
        //var_dump($profile);
       
       $filename = "/var/www/emoncms/profiles/".$profile["filename"];
     
       if(file_exists( $filename )) 
       {
        
                $ts = Array();
                $axis1 = Array();
                $axis2 = Array();
                
                $myfile = fopen($filename, "r") or die("Unable to open file!");
                $duration=fgets($myfile);
                $poly=Array();
                $poly[0]=fgets($myfile);
                $poly[1]=fgets($myfile);
                $poly[2]=fgets($myfile);
                $poly[3]=fgets($myfile);	
                fclose($myfile);
            
                $step=$duration/10;
                $x=0;
                $axis1[]=0;
                $axis2[]=0;
                while($x<$duration)
                {
                    
                    $value=$poly[0]*pow($x,3)+$poly[1]*pow($x,2)+$poly[2]*$x;
                    $axis1[]=$x;
                    $axis2[]=$value;
                    $x=$x+$step;
                    }
                $ts["x"]=$axis1;
                $ts["y"]=$axis2;
                
       }
       else{
           $ts["x"]=array(0);
            $ts["y"]=array(0);
           }
                $ts["type"]="scatter";
                $ts["modes"]=$profile["modes"];
                return $ts;
		}	
	
	
	public function getProfileInfo($name)
	{
		
		$profile = Array();
		if($this->redis)
		  
			  $profile = $this->redis->hGetAll($name);
		
		return $profile;
		}
		
    
     
	 
    
     public function getProfile($userid, $deviceid, $modeid, $type)
    {
		
	   if($type==3)
		$filename="u".$userid."d".$deviceid."m".$modeid.".prof";   
	   else if($type==1)
	   {	
		$filename="u".$userid."d".$deviceid."m".$modeid.".prof";
		}
		else
			$filename="d".$deviceid."m".$modeid.".prof";
	   
	   if(file_exists("/var/www/emoncms/profiles/".$filename))
		   {
			$result["success"]=true;
			$result["profile"]=$filename;
			$values = $this->get_energy_duration("/var/www/emoncms/profiles/".$filename);
			$result["duration"] = floatval($values["duration"]);
			$result["energy"] = floatval($values["energy"]);
		   }
		 else
		 {
			$result["success"]=false;
			$result["message"]="profile not available";
	      }
	      return $result;
    }
	
	public function get_energy_duration($filename)
	{
		$myfile = fopen($filename, "r") or die("Unable to open file!");
		$x=fgets($myfile);
		$poly=Array();
		$poly[0]=fgets($myfile);
		$poly[1]=fgets($myfile);
		$poly[2]=fgets($myfile);
		$poly[3]=fgets($myfile);	
		fclose($myfile);
		$result = array();
		$result["duration"] = $x;
		$result["energy"] = $poly[0]*pow($x,3)+$poly[1]*pow($x,2)+$poly[2]*$x;
		return $result;
		}
    
   
    
    
    public function learn($userid,$node, $name, $time_now, $value)
        {
			/*
        //$dev=getdevice($namenode,$name)
        $dev=1;   
		
		//Get Running Task  
		$qresult = $this->mysqli->query("SELECT * FROM user_tasks  WHERE userid=$userid and deviceid=$dev and status=4");
		//$this->log->info("SELECT * FROM user_tasks  WHERE userid=$userid and deviceid=$dev and status=4");
		if($task = $qresult->fetch_array()) 	
        //$task=getRunningTask($device)<-- {'taskID':,'device':,'mode':,'ast':, 'ltime':}
        if($task['ast']){
         
        $t=$time_now-$task['ast'];
        $mode=$task['modeid'];
        
        $path="/var/www/emoncms/profiles";
        $filename="d".$dev."m".$mode;
        $cmd= "echo '$t,$value'  >>".$path."/".$filename.".temp";
        
		$this->log->info($cmd." ".$task['ast']);        
        shell_exec($cmd);
        
        if($value>0.01)
         {
         $ltime=$time_now;  
         $this->log->info("UPDATE user_tasks set ltime=$ltime where id=".$task['id']);
         $this->mysqli->query("UPDATE user_tasks set ltime=$ltime where id=".$task['id']);
         }
         else{
           if($time_now-$task['ltime']>20000)
        	   {
               	  $this->mysqli->query("UPDATE user_tasks set status=5 where id="+$task['id']);  
                  shell_exec("python /var/www/emoncms/Modules/mas/bin/updateprof.py ".$filename);
         		}
          	}
         
	    }
        
        $this->log->info("mas.learn() received userid= $userid time=$time_now, value=$value, node=$node, name=$name ");
        * */
}

/*
    // USES: redis input & user
    public function create_driver($userid, $nodeid, $name)
    {
        $userid = (int) $userid;
        $nodeid = (int) $nodeid;

        if ($nodeid<32)
        {

          $name = preg_replace('/[^\w\s-.]/','',$name);
          $this->mysqli->query("INSERT INTO input (userid,name,nodeid) VALUES ('$userid','$name','$nodeid')");
          
          $id = $this->mysqli->insert_id;
          
          $this->redis->sAdd("user:inputs:$userid", $id);
	        $this->redis->hMSet("input:$id",array('id'=>$id,'nodeid'=>$nodeid,'name'=>$name,'description'=>"", 'processList'=>"")); 
	        
	      }
	      
	      return $id;     
    }
    */
    
    public function exists($id)
    {
      $id = (int) $id;
      $result = $this->mysqli->query("SELECT id FROM tasks WHERE `id` = '$id'");
      if ($result->num_rows > 0) return true; else return false;
    }
    
    
/*
    // USES: redis input
    public function set_timevalue($id, $time, $value)
    {
        $id = (int) $id;
        $time = (int) $time;
        $value = (float) $value;

        // $time = date("Y-n-j H:i:s", $time);
        // $this->mysqli->query("UPDATE input SET time='$time', value = '$value' WHERE id = '$id'");
        
        $this->redis->hMset("input:lastvalue:$id", array('value' => $value, 'time' => $time));
    }

*/
    // used in conjunction with controller before calling another method
    public function belongs_to_user($userid, $taskid)
    {
        $userid = (int) $userid;
        $taskid = (int) $taskid;

        $result = $this->mysqli->query("SELECT id FROM tasks WHERE userid = '$userid' AND id = '$taskid'");
        if ($result->fetch_array()) return true; else return false;
    }
/*
    // USES: redis input
    private function set_processlist($id, $processlist)
    {
      // CHECK REDIS
      $this->redis->hset("input:$id",'processList',$processlist);
      $this->mysqli->query("UPDATE input SET processList = '$processlist' WHERE id='$id'");
      
    }


    // USES: redis input
  */
  
  
   public function set_fields($id,$jsonstring)
    {
		
        $id = intval($id);
       
        $json=strtr ($jsonstring, array ("'" => '"'));
        $fields = json_decode(stripslashes($json),true);
        $success=false;
             
         foreach ( $fields as $key => $value ) {
			 
			    if($key=='AST')  $this->fakeschedule($id, $value);
			 
			 
				 if($key=='status')
				    $this->mysqli->query("UPDATE  user_tasks set status='$value' where  id=$id");
				 
			 
				 $this->mysqli->query("UPDATE user_task_par set value='$value' where taskid=$id and name='$key'");
				 
				 if($this->mysqli->affected_rows==0){
				     $this->mysqli->query("INSERT INTO user_task_par VALUES(NULL, $id, '$key', '$value')");
					 if($this->mysqli->affected_rows==0)  
						$success=true;
					}
				 else 
				   $success=true;
				}	
				$success==true? $success='success':  $success='error';
		 
          return "{'response': 'updateTask', 'result': '$success', 'taskID': '$id'}";
	}
  
  
    public function update($id,$jsonstring)
    {
        $id = intval($id);
       
        $json=strtr ($jsonstring, array ("'" => '"'));
        $fields = json_decode(stripslashes($json),true);
        $success=false;
        $update_feeds=False;     
			 
         foreach ( $fields as $key => $value ) {
			 
			   
				 if($key=='status')
				 {  
                      $now = new DateTime();
						if($value==5)
							{
								$update_feeds=True;
								$result = Array();
								$qresult = $this->mysqli->query("select userid,deviceid,modeid from  user_tasks  where id=".$id);
								if ($qresult->num_rows > 0 )
								    {
										$task=$qresult->fetch_object();
                                    $this->endLoad($task->userid,$id);
									}
                            
                            
							}
                      $this->mysqli->query("UPDATE  user_tasks set status='$value',ltime=".$now->getTimestamp()." where  id=$id");
						
												
				 }
				if($key=='vendor')
				 {  
						$this->mysqli->query("UPDATE  user_tasks set vendor='$value' where  id=$id");
									
				 }
				 
				 if($key=='AST'){
					 
					$value1 = strtotime($value);
				 	$this->mysqli->query("UPDATE  user_tasks set ast=$value1 where  id=$id");
				 	
				}
			 
				 $this->mysqli->query("UPDATE user_task_par set value='$value' where taskid=$id and name='$key'");
				 
				 if($this->mysqli->affected_rows==0){
				     $this->mysqli->query("INSERT INTO user_task_par VALUES(NULL, $id, '$key', '$value')");
					 if($this->mysqli->affected_rows==0)  
						$success=true;
					}
				 else 
				   $success=true;
				}	
				$success==true? $success='success':  $success='error';
		 
		   return "{'response': 'updateTask', 'result': '$success', 'taskID': '$id'}";
        
        
/*
        // Repeat this line changing the field name to add fields that can be updated:
        if (isset($fields->description)) $array[] = "`description` = '".preg_replace('/[^\w\s-]/','',$fields->description)."'";
        if (isset($fields->name)) $array[] = "`name` = '".preg_replace('/[^\w\s-.]/','',$fields->name)."'";
        if (isset($fields->status)) $array[] = "`status` = '".$fields->status."'";
        // Convert to a comma seperated string for the mysql query
        
        
        
        $fieldstr = implode(",",$array);
        * 
        $this->mysqli->query("UPDATE user_drivers SET ".$fieldstr." WHERE `id` = '$id'");
        
        // CHECK REDIS?
        // UPDATE REDIS
        if (isset($fields->name)) $this->redis->hset("driver:$id",'name',$fields->name);
        if (isset($fields->description)) $this->redis->hset("driver:$id",'description',$fields->description);        
        if (isset($fields->status)) $this->redis->hset("driver:$id",'status',$fields->status);        
        */
        
        
       
    }
    
    
    
    
    public function add($userid, $jsonstring)
    {
		 
		 
		
		   $json=strtr ($jsonstring, array ("'" => '"'));
		   $task = json_decode(stripslashes($json), true);
		
		   if($userid == null)
		    $userid=1;
		   
		   $u=$userid;
		   $d=$task['deviceID'];
		   $m=$task['mode'];
		  
	
			#check if there are load already planned for this devie
			$query = "SELECT id from user_tasks where  status<=1 and deviceid=$d";
			$qresult = $this->mysqli->query($query);
			if ($qresult->fetch_object())
			{
				$response=array();
				$response["response"]="addTask";
				$response["result"]="error";
				$response["message"]="there is a load already planned for this device";
				return $response;
			}
		   $valid=true;
		   if($task['execution_type']==="single_run")
		    {  $type=1;
				//$this->devices->set_device_status($userid, $d, 0);
			
	
			}
			else if($task['execution_type']==="periodic_run")
            {
				$type=2;
				}
			else if($task['execution_type']==="e_car")
            {
				$type=3;
				}
             else
               {
				$message="'execution type not supported yet'";
				$valid=false;
				  }
				  
			error_log("mas:add task valid"+$valid);	  
			if($valid){
				$output=$this->getProfile($u,$d,$m,$type);
			    if($output["success"]==true)
			     { 
				   $task['status']=-1;
				   $task['profile']= "".$output["profile"];
				   $task['energy']= $output["energy"];
				   $task['duration']= $output["duration"];
				   $task['AST'] = 	$task['LST'];
				   $startime = new DateTime($task['AST']);	
				   $task['AET']	=   date('Y-m-d H:i',$startime->add(new DateInterval('PT'.$output["duration"].'S'))->getTimestamp());
				  }
				 else
					{
						$task['status'] = -1;
						$task['AST']=$task["EST"];
						$task['energy']= -1;
						$task['duration']= -1;
						$task['AET']="UNDEFINED";	
					}
				
				
				
				$result = $this->mysqli->query("SHOW TABLE STATUS LIKE 'user_tasks'");
				$row = $result->fetch_array(1);
				$nextId = $row['Auto_increment']; 
				$task['status'] = 0; 
			    //$this->log->info("INSERT INTO user_tasks (id, type, status, deviceid, modeid, userid) VALUES ($nextId, $type, $status,".$task['deviceID'].",".$task['mode'].",".$userid.")");
				$inserts=$this->mysqli->query("INSERT INTO user_tasks (id, type, status, deviceid, modeid, userid,energy,duration,ast) VALUES ($nextId, $type,".$task['status'].",".$task['deviceID'].",".$task['mode'].",". $userid.",".$task['energy'].",".$task['duration'].",".strtotime($task['AST']).")");
               error_log("mas:INSERT INTO user_tasks (id, type, status, deviceid, modeid, userid,energy,duration,ast) VALUES ($nextId, $type,".$task['status'].",".$task['deviceID'].",".$task['mode'].",". $userid.",".$task['energy'].",".$task['duration'].",".strtotime($task['AST']).")");
             
				
                
				if(!$inserts)
				{
					$valid=false;
					$message = 'INVALId query:' .mysql_error()."\n";		
				}
				else 
					{
						
					 foreach ($task as $key => $value)  
						$this->mysqli->query("INSERT INTO user_task_par VALUES(NULL, $nextId, '$key', '$value')");
					  
					  $this->mysqli->query("INSERT INTO user_task_par VALUES(NULL, $nextId, 'AST', '".$task['AST']."')");
					  $this->mysqli->query("INSERT INTO user_task_par VALUES(NULL, $nextId, 'AET', '".$task['AET']."')");
					}
					
					
					/*
					$jsonstring=json_encode($task);
					$jsonstring=strtr ($jsonstring, array ('"' => "'"));
					$url='http://localhost:8808/add.json?taskid='.$nextId.'&jsons="'.urlencode($jsonstring).'"';
					error_log("[add_task]".$url);
					$this->log->info($url);
					$this->restRequest($url);	
					*/
					
					
					$response=array();
					$response["response"]="addTask";
					$response["result"]="success";
					$response["taskID"]="$nextId";

               return $response;
				}
			
			
			$response=array();
			$response["response"]="addTask";
			$response["result"]="error";
			$response["message"]="$message";
			return $response;
		}


 public function getTaskStatus($taskid)
 {
	  $qresult = $this->mysqli->query("select id, type, status  from user_tasks where id=$taskid ;");
      
       
          
         if($row = $qresult->fetch_object()) 
          return $row->status;
         else 
            return -1;
	 }
 
  public function getTask($userid, $taskid)
    {     
        $userid = (int) $userid;
       
        $qresult = $this->mysqli->query("select id, type, status  from user_tasks where id=$taskid ;");
      
       
         $response   =array();
         $response['response'] = 'getTask';
         if($row = $qresult->fetch_object()) 
         {
           
           $response['result'] = 'success';
           $parameters = array();
           $parameters['id']= $row->id;
           $parameters['status']= $row->status;
           $parameters['type']= $row->type;
           
			//$response="{'response': 'getTask', 'result':'success', 'task': {'id': $row->id, 'status': $row->status, 'type': $row->type, ";
        
			$qresult = $this->mysqli->query("select name, value  from user_task_par where taskid=$taskid;");
       
         while($row = $qresult->fetch_object())
		{
			$parameters[$row->name] = $row->value;
            
			//$response=$response.", '".$row->name."': '".$row->value."'";
			}
			$response['task']=$parameters;
         //$response=$response."}}";
	 }
	 else
	  
         {
             $response['result']='error';
             $response['message']='taskid $taskid not found';
            //$response = "{'response': 'getTask', 'result': 'error', 'message': 'taskid $taskid not found'}";
        
         }
        return $response;
    }

    public function getlist($userid,$jsonstring)
    {   
		if($jsonstring!=null)
		{
		   $json=strtr ($jsonstring, array ("'" => '"'));
		   $loptions = json_decode($json, true);
	
		   
		}
		
        $userid = (int) $userid;
        $query="select id,type, status from user_tasks where userid= '$userid'";
        if(isset($loptions) and array_key_exists("status", $loptions))
             if($loptions['status'] == 10)
			    $query=$query." and status < 4";  		     
               else             
				$query=$query." and status=".$loptions['status'];  
        
        if(isset($loptions) and array_key_exists("execution_type", $loptions))
    			{
					$type = 0;				
				   if($loptions['execution_type']==="single_run")
					 $type=1;
				   else if($loptions['execution_type']==="periodic_run")
					 $type=2;
				   else if($loptions['execution_type']==="e_car")
					 $type=3;
				   if ($type>0) 
					$query=$query." and type=".$type;
				}
    	if(isset($loptions) and array_key_exists("deviceID", $loptions))
    			$query=$query." and deviceid=".$loptions['deviceID'];    
       
        $query = $query." order by id desc limit 0,20"; 
        $qresult = $this->mysqli->query($query);
        $userid = (int) $userid;
        /*if (!$this->redis->exists("user:drivers:$userid"))*/ 
        $this->load_to_redis($userid);
        
        /*$tasks = array();
        $driverids = $this->redis->sMembers("user:drivers:$userid");
        */
        $ra =  array("response"=>"listTasks", "result"=>"success");
        $tasks=array();
        
        #Adrian asked to limit the number of periodic task to 4
        $periodictasks = 0;
         while($row = $qresult->fetch_object())
		{
           if(!(isset($loptions) and array_key_exists("all",$loptions)))
           { 
                   if (($row->type == 2) and  ($periodictasks > 4))
                          continue;
                   elseif ($row->type == 2)
                        $periodictasks += 1;
                        
           }
             
           
           
            
			$task=array();
			$task["id"]=$row->id; 
           $task["type"]=$row->type; 
			$task["status"]=$row->status; 
			
			if(isset($loptions) and array_key_exists("info",$loptions))
			 if($loptions['info']==true)
			     {
					 
					 $presults = $this->mysqli->query("select name, value  from user_task_par where taskid=$row->id;");
					  while($par = $presults->fetch_object())
								$task[$par->name]=$par->value;
							
				}    
     		$tasks[]=$task;
			
			
			
		}
		   $ra["tasks"]=$tasks;
        
        /*
        foreach ($taksid as $id)
        {
          $row = $this->redis->hGetAll("driver:$id");
          
          $lastvalue = $this->redis->hmget("input:lastvalue:$id",array('time','value'));
          $row['time'] = $lastvalue['time'];
          $row['value'] = $lastvalue['value'];
          $drivers[] = $row;
        }*/
      
        
        return $ra;
        
    }
    
   
    
    
    
/*
    // USES: redis input
    public function get_name($id)
    {
        // LOAD REDIS
        $id = (int) $id;
        if (!$this->redis->exists("input:$id")) $this->load_input_to_redis($id);
        return $this->redis->hget("input:$id",'name');
    }

    // USES: redis input
    public function get_processlist($id)
    {
        // LOAD REDIS
        $id = (int) $id;
        if (!$this->redis->exists("input:$id")) $this->load_input_to_redis($id);
        return $this->redis->hget("input:$id",'processList');
    }
    
    public function get_last_value($id)
    {
      $id = (int) $id;
      return $this->redis->hget("input:lastvalue:$id",'value');
    }
    

    //-----------------------------------------------------------------------------------------------
    // Gets the inputs process list and converts id's into descriptive text
    //-----------------------------------------------------------------------------------------------
    // USES: redis input
    public function get_processlist_desc($process_class,$id)
    {
        $id = (int) $id;
        $process_list = $this->get_processlist($id);
        // Get the input's process list

        $list = array();
        if ($process_list)
        {
            $array = explode(",", $process_list);
            // input process list is comma seperated
            foreach ($array as $row)// For all input processes
            {
                $row = explode(":", $row);
                // Divide into process id and arg
                $processid = $row[0];
                $arg = $row[1];
                // Named variables
                $process = $process_class->get_process($processid);
                // gets process details of id given

                $processDescription = $process[0];
                // gets process description
                if ($process[1] == ProcessArg::INPUTID)
                  $arg = $this->get_name($arg);
                // if input: get input name
                elseif ($process[1] == ProcessArg::FEEDID)
                  $arg = $this->feed->get_field($arg,'name');
                // if feed: get feed name

                $list[] = array(
                  $processDescription,
                  $arg
                );
                // Populate list array
            }
        }
        return $list;
    }

    // USES: redis input & user*/
    public function delete($userid, $taskid)
    {
		  
		   $response= array();
		   $valid=true;  
		   $message = "taskid is not set";
		   if(isset($taskid))
		   {
			 $qresult=$this->mysqli->query("select status from user_tasks where userid=$userid and id=$taskid");
			 
			 if( $raw=$qresult->fetch_object())
			 {
				 $status=$raw->status;
				 if($status<=1)  //not running or failed
				 {
					$this->mysqli->query("delete from user_task_par where  taskid=$taskid");  
					$qresult=$this->mysqli->query("delete from user_tasks where userid=$userid and id=$taskid");
		   
					$url='http://localhost:8808/del.json?taskid='.$taskid;
					
					$this->restRequest($url);
			    
			    
					$response["result"]="success";
					$response["response"]="removeTask";
					$response["taskId"]="$taskid";
               return $response;
				 }
				 else
					  $message=" status of load $taskid is running";
			}
           else
				 $message="taskid $taskid not found";
			}
			
				$response["result"]="error";
				$response["response"]="removeTask";
				$response["taskId"]="$message";
               return $response;
    }
    
     public function execute($userid, $taskid)
    {
		   $response= array();
		   $this->set_fields($taskid,'{"status":4}');
		   
		   if(isset($taskid))
		   {
			
		   $result = $this->mysqli->query("SELECT value FROM user_task_par WHERE taskid=$taskid and name='deviceID'");
		  
		  if( $data = $result->fetch_object())  
           {
			    
				$deviceid=$data->value;
				$AST=shell_exec("date ");
			    $AST=rtrim($AST,"\n");
                $baseurl='http://localhost/virtualDevices/device.php?json=';
                $url=$baseurl.urlencode('{"cmd": "set", "device": '.$deviceid.',"parameters": [{"name": "startedon", "value": "'.$AST.'"}]}');
				error_log("seturl".$url);
				$curl = curl_init();
				// Set some options - we are passing in a useragent too here
				curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $url
						));
				// Send the request & save response to $resp
				$resp = curl_exec($curl);
				// Close request to clear up some resources
				curl_close($curl); 
			    
			    
			    
				$response["result"]="success";
				$response["response"]="executeTask";
				$response["taskId"]="$taskid";
               return $response;
           }
           else
				 $message="taskid $taskid not found";
			}
		
			$response["result"]="error";
			$response["response"]="executeTask";
			$response["message"]="$message";
			
		  
			return $response;
    }
    
    // Redis cache loaders

    private function load_input_to_redis($inputid)
    {
      $result = $this->mysqli->query("SELECT id,nodeid,name,description,processList FROM input WHERE `id` = '$inputid'");
      $row = $result->fetch_object();
      
      $this->redis->sAdd("user:inputs:$userid", $row->id);
      $this->redis->hMSet("input:$row->id",array(
        'id'=>$row->id,
        'nodeid'=>$row->nodeid,
        'name'=>$row->name,
        'description'=>$row->description,
        'processList'=>$row->processList
      ));
      
    }
   
   
 

	private function masinvoke($url)
	  {
		  
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// Close request to clear up some resources
		curl_close($curl);
		$resp=strtr ($resp, array ("'" => '"'));
		if($resp == "") return 0;
		
		$obj = json_decode(stripslashes($resp));
		if($obj->result == "success")
			return 1;
		else 
			return 0;
		}



    private function load_to_redis($userid)
    {
		/*
        $result = $this->mysqli->query("select user_drivers.id, type, name, status, description from user_drivers, driver where userid= $userid and user_drivers.type=driver.id;");
      
              
        while($row = $result->fetch_object()){
      
        $this->redis->sAdd("user:drivers:$userid", $row->id);    
	    $this->redis->hMSet("driver:$row->id",array(
	        'id'=>$row->id,
	        'type'=>$row->type,
	        'name'=>$row->name,
	        'description'=>$row->description,
	        'status'=>$row->status,
	        'userid'=>$userid
	        
	      ));
      }*/
    }
  /*******/
  private function saveLoadData($filename, $deviceid, $mode)
  {
	  
	   // Get cURL resource
	    
		//$url="http://cloud.cossmic.eu/cossmic/virtualdevices/device.php?json=".urlencode("{'cmd': 'getprofile', 'deviceid': $deviceid, 'mode': $mode, 'unit': 'e'}");
		$baseurl="http://localhost/virtualDevices/device.php?json=";
		$url=$baseurl.urlencode("{'cmd': 'getprofile', 'deviceid': $deviceid, 'mode': '".$mode."'}");
		
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt($curl, CURLOPT_USERPWD, "cossmichg:microgrid");
		curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $url
				));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// Close request to clear up some resources
		curl_close($curl); 
		$loadfile = fopen($filename, "w");
	   	fwrite($loadfile, $resp);
		fclose($loadfile);
	  }
  
  /*******/
  public function fakeschedule($taskid,$AST)
  {
	  $result = $this->mysqli->query("SELECT username FROM user_tasks, users WHERE user_tasks.id = $taskid and users.id=user_tasks.userid");
      $row = $result->fetch_object();
      $username = $row->username;
	  $filename = $this->loadsdir."/loads/".$username."-".$taskid.".metaload";
	  copy($filename,"/tmp/".$username."-".$taskid.".metaload");
	  $temp=explode(":", $AST);
	  $AST=$temp[0]*3600+$temp[1]*60;
	  
	  file_put_contents("/tmp/".$username."-".$taskid.".metaload", "AST ".$AST, FILE_APPEND | LOCK_EX);
	  
	  copy("/tmp/".$username."-".$taskid.".metaload", $filename = $this->loadsdir."/schedule/".$username."-".$taskid.".metaload");
	  
	  }

   

   public function settings()
   {
	   $result = $this->mysqli->query("SELECT * FROM mas_par");
       $pars= array();
        while($row = $result->fetch_object())
        {
			$pars[] = $row;
			}
		return $pars;
       
	   }
	   
	   public function save_settings($id, $jsonstring)
    {
		
        
        $json=strtr ($jsonstring, array ("'" => '"'));
        $fields = json_decode(stripslashes($json),true);
        $success=false;
             
         	 
			     
				 $this->mysqli->query("UPDATE mas_par set value='".$fields['value']."'  where  id=$id");
				 
				 if($this->mysqli->affected_rows==0){
					
				     $this->mysqli->query("INSERT INTO mas_par VALUES($id, '".$fields['name']."', '".$fields['value']."')");
					 if($this->mysqli->affected_rows!=0)  
						$success=true;
					}
				 else 
				   $success=true;
					
				$success==true? $success='success':  $success='error';
		 
		   
          return array("response"=> "savesettings", "result"=> "$success", "parameter"=>"INSERT INTO mas_par VALUES($id, '".$fields['name']."', '".$fields['value']."')");
	}
	   
	   
	   private function  getDropboxdir()
    {/*
		 $this->mysqli->query("SELECT value from mas_par   where  id=0");
		 $row = $result->fetch_object();
		 while($row = $result->fetch_object())
          {
			  if("rows[0]"!= "Not Set")
			   
			   return 
			}
		*/		 
	}
	
	public function status($userid)
	{
		$result = array();
		
		if(file_exists("/tmp/spade.pid"))
		   $result["spade"]=1;
		else
		   $result["spade"]=0;
		
		if(file_exists("/tmp/tm.pid"))
		   $result["tm"]=1;
		else
		   $result["tm"]=0;
		   
		if(file_exists("/tmp/am.pid"))
		   $result["am"]=1;
		else
		   $result["am"]=0;
		
		return $result;
		
		}
	
	public function start($userid, $agent)
	{
		
		$result = array();
		$daemons= array();
		$result ["tm"] =1;
		if(!file_exists("/tmp/taskmanager.pid"))
			{
				//shell_exec("/var/www/emoncms/Modules/mas/bin/mas.sh stop");
				//shell_exec("/var/www/emoncms/Modules/mas/bin/mas.sh start");
				//if(!file_exists("/tmp/taskmanager.pid"))
					$result ["tm"] =0;
			}
		 
		 return $result;
		
		}
		
	public function stop($userid, $alg)
	{
		
		$records = $this->mysqli->query("SELECT username, apikey_write FROM users WHERE id=".$userid);
		$row=$records->fetch_object();
		$username=$row->username;
		$apikey=$row->apikey_write;
		$result = array();
		$result ["error"]="";
		$result ["result"] ="0";
		if($alg=="random")
		{
			if(!file_exists("/tmp/randomsched.pid"))
			{
				$result ["result"] ="3";
				$result ["error"] =$result ["error"]." random scheduler not running";
				
			}
		else	
			{
				shell_exec("/var/www/emoncms/Modules/mas/bin/randomsched.sh stop");	 
				
			}
		}
		if(!file_exists("/tmp/".$username.".pid"))
		{
			$result ["result"] ="3";
			$result ["error"] =$result ["error"]." updater not running";
			
			}
		else
		  {
			shell_exec("/var/www/emoncms/Modules/mas/bin/schedulerd.sh stop $username $apikey");
			$result ["error"]+=" MAS stopped";
			return $result;
		  }
		  
		return $result;
		
		}	
		
	
	
	public function restRequest($url)
	{
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $url
				));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// Close request to clear up some resources
		curl_close($curl);
		$resp=strtr ($resp, array ("'" => '"'));
		
		return $resp;
	}
	
	public function deviceConnected($userid, $jsonstring)
	{
		//http://host/emoncms/mas/connection.json?json={device:137,'connection':'on'}
		$json=strtr ($jsonstring, array ("'" => '"'));
        $fields = json_decode(stripslashes($json),true);
        $deviceid = $fields['device'];
        $connection = $fields['connection'];
       
        $response=array();
		$response["cmd"]="connection";
		
		
			
        
        if($deviceid and $connection)
		{
		   if($connection==='off')
		    	{
					//delete current loads
					$tasks = $this->getlist($userid,"{'deviceID':$deviceid}");	
					$tasks=$tasks["tasks"];
					if($tasks)
						foreach ($tasks as $task)
						{
							$this->delete($task["id"]);
						}
					$response["result"]="success";
					$response["message"]="e-car tasks deleted";
				
					}
           $device = $this->devices->get_device_by_deviceid($userid,$deviceid);
	       $template=$this->devices->get_template($userid,$device["templateid"]);
	       
        if($template->operatingType==="e-car")
          {
			
			$configuration = array();
			$parameters=$this->devices->get_parameters($deviceid);
			
			
			foreach ($parameters as $parameter)
				{
					if($parameter["name"]==="minimum_energy_target")
						$configuration['minimum_energy_target']=$parameter["value"];
					else if($parameter["name"]==="capacity")		
						$configuration['capacity']=$parameter["value"];				
					else if($parameter["name"]==="MaxChargingPower")			
						$configuration['MaxChargingPower']=$parameter["value"];
					else if($parameter["name"]==="target_deadline")			
						$configuration['target_deadline']=$parameter["value"];
					else if($parameter["name"]==="energy_target")			
						$configuration['energy_target']=$parameter["value"];

			}
			
			if (array_key_exists("configuration",$fields))
			  {
				  $override=$fields['configuration'];
				 
				  foreach ($override as  $key => $parameter)
						$configuration[$key]=$parameter;
				
			 }
				
				
			$result = $this->scheduleECar0($userid, $deviceid, $configuration);			
			return $result;
           }
           else
           {
			   $response["result"]="failure";
			   $response["message"]="device type is not e-car";
			   }
          }
        else
        {
			$response["result"]="failure";
			$response["message"]="request format error";
			}
			
		
		return $response;
	}
        
        
	public function scheduleECar0($userid, $deviceid, $configuration)
	{
		//delete current loads
		$tasks = $this->getlist($userid,"{'deviceID':$deviceid}");	
		$tasks=$tasks["tasks"];
		
		if($tasks)
		foreach ($tasks as $task)
		{
			$this->delete($userid, $task["id"]);
			}
		
		//generate new loads
		//create load file 0
		//echo var_dump($configuration);
		 
		$mintarget=$configuration['minimum_energy_target']*$configuration['capacity']/100; 
		$duration = $mintarget*3600/$configuration['MaxChargingPower'];
		//$LST=$now+$deadline-$duration;
		$now = new DateTime();
		$EST =  $now->getTimestamp();
		$now->add(new DateInterval('PT'.$duration.'S')); // adds 674165 secs
		$LST = date('Y-m-d H:i',$now->getTimestamp());
		
		//create file for fastest charge to mintarget; 
		$filename = "u".$userid."d".$deviceid."m0.prof";
		$fp = fopen("/var/www/emoncms/profiles/".$filename, 'w');
		fputcsv($fp,[0,0]);
		fputcsv($fp,[$duration,$mintarget]);
		fclose($fp);
		
		$result = array();
	
		$load = "{'EST': '$EST','LST': '$LST','deviceID': $deviceid, 'execution_type': 'e_car', 'mode' : 0,'profile':'$filename'}";
		//echo $load;
		
		$result[]=$this->add($userid,$load);
		
		//create profile to target; 
		$deadline = new DateTime($configuration['target_deadline']);
		$duration = $deadline->getTimestamp()-$now->getTimestamp();
		
		
		$filename = "u".$userid."d".$deviceid."m1.prof";
		$fp = fopen("/var/www/emoncms/profiles/".$filename, 'w');
		fputcsv($fp,[0,0]);
		fputcsv($fp,[$duration,$configuration['energy_target']*$configuration['capacity']/100]);
		fclose($fp);
		
		$EST=$LST;
		$LST=$configuration['target_deadline'];
		
		$load = "{'EST': '$EST','LST': '$LST','deviceID': $deviceid, 'execution_type': 'e_car', 'mode' : 1,'profile':'$filename'}";
		$result[]=$this->add($userid,$load);
		
		return $result;	
		}

      public function setup_mas_feeds($userid,$nodeid=0)
		{
         /*
          * $nodeid was originally set =-1 to use device manager. The last change excludes devicemnanager
          * public function setup_mas_feeds($userid,$nodeid=-1)
		
        	if( $nodeid < 0)
            {
                $output = $this->devices->get_nodeid($userid,"agents","0");
                if ($output["success"]==False)
                {
                    $output = $this->devices->register_node($userid, "agents", "0", ["pv2neighborhood","neighborhood2household","reward"]);
                    
                    if($output["success"]==True)
                        {
                            $nodeid = $output["nodeid"];
                        }
                    else return -1;
                }
            }
             * */
                
            
                    $dbinputs = $this->input->get_inputs($userid);
                    
                    if(array_key_exists($nodeid,$dbinputs) and array_key_exists('neighborhood2household',$dbinputs[$nodeid])) return $nodeid;
                        
                        
                        
                    
                    //CREATE MAS FEEDS
                    $inputid = $this->input->create_input($userid, $nodeid, "neighborhood2household");
                    #$inputid = $dbinputs[$nodeid]['neighborhood2household']['id'];
                    $dbinputs[$nodeid]['neighborhood2household'] = true;
                    $dbinputs[$nodeid]['neighborhood2household'] = array('id'=>$inputid);
            
                    //Log energy to feed: Realtime(1), phptimeseries(2)
                    $feedid=$this->feed->create($userid,"","neighborhood2household_kwh",1,2,json_decode('{"interval":180}'));
                    $this->feed->set_feed_fields($feedid["feedid"], '{"tag":"Node: '.$nodeid.'"}');
                    $this->input->add_process($this->process, $userid, $inputid,35,$inputid);
                    $this->input->add_process($this->process, $userid, $inputid,14,$feedid["feedid"]);
                            
                    //Kwh to KWhd
                    $feedid=$this->feed->create($userid,"","neighborhood2household_kwhd",2,2,json_decode('{"interval":86400}'));
                    $this->input->add_process($this->process, $userid, $inputid,5,$feedid["feedid"]);
                    $this->feed->set_feed_fields($feedid["feedid"], '{"tag":"Node: '.$nodeid.'"}');
                    
                    $inputid = $this->input->create_input($userid, $nodeid, "pv2neighborhood");
                    $dbinputs[$nodeid]['pv2neighborhood'] = true;
                    $dbinputs[$nodeid]['pv2neighborhood'] = array('id'=>$inputid);
            
                                
                    //Log energy to feed: Realtime(1), phptimeseries(2)
                    $feedid=$this->feed->create($userid,"","pv2neighborhood_kwh",1,2,json_decode('{"interval":180}'));
                    $this->feed->set_feed_fields($feedid["feedid"], '{"tag":"Node: '.$nodeid.'"}');
                    $this->input->add_process($this->process, $userid, $inputid,35,$inputid);
                    $this->input->add_process($this->process, $userid, $inputid,1,$feedid["feedid"]);
                            
                    //Kwh to KWhd
                    $feedid=$this->feed->create($userid,"","pv2neighborhood_kwhd",2,2,json_decode('{"interval":86400}'));
                    $this->input->add_process($this->process, $userid, $inputid,5,$feedid["feedid"]);
                    $this->feed->set_feed_fields($feedid["feedid"], '{"tag":"Node: '.$nodeid.'"}');
                    
                    
                    $inputid = $this->input->create_input($userid, $nodeid, "reward");
                    $dbinputs[$nodeid]['reward'] = true;
                    $dbinputs[$nodeid]['reward'] = array('id'=>$inputid);
                    
                    //Log energy to feed: Realtime(1), phptimeseries(2)
                    $feedid=$this->feed->create($userid,"","reward",1,2,json_decode('{"interval":180}'));
                    $this->feed->set_feed_fields($feedid["feedid"], '{"tag":"Node: '.$nodeid.'"}');
                    $this->input->add_process($this->process, $userid, $inputid,35,$inputid);
                    $this->input->add_process($this->process, $userid, $inputid,1,$feedid["feedid"]);
                            
                    //Kwh to KWhd
                    $feedid=$this->feed->create($userid,"","reward_d",2,2,json_decode('{"interval":86400}'));
                    $this->input->add_process($this->process, $userid, $inputid,5,$feedid["feedid"]);
                    $this->feed->set_feed_fields($feedid["feedid"], '{"tag":"Node: '.$nodeid.'"}');
                    
            
				
				
			return $nodeid;
			}
		
		public function  update_neighborhood2household($userid,$energy)
		{
            $nodeid = 0;
            $now = time();
             $dbinputs = $this->input->get_inputs($userid);
             $inputid = $dbinputs[$nodeid]['neighborhood2household']['id'];
             $this->input->set_timevalue($dbinputs[$nodeid]['neighborhood2household']['id'],$now,$energy);
                if ($dbinputs[$nodeid]['neighborhood2household']['processList']) 
                    { 
                        $tmp[] = array('value'=>$energy,'processList'=>$dbinputs[$nodeid]['neighborhood2household']['processList']);
                        foreach ($tmp as $i) $this->process->input($now,$i['value'],$i['processList']);
                    }
       
			}
                
    
                
                
                
                
            public function auto($automata){ 
                
                //check if it exist a load ready to be scheduled for this device.
              $response=Array(); 
				$response["result"]="failure";
                
                $query = "select id from user_tasks where  status=-1 and deviceid=".$automata['deviceid'];
                $qresult = $this->mysqli->query($query);
                
                   
                if ($qresult->num_rows > 0)
				 {  
					 $row = $qresult->fetch_object();
                        
					 $this->submitLoad($row->id);
                
					 $response["result"]="success";
                 }
                else{
					//get devie paramaters
					$parameters=$this->devices->get_parameters($automata['deviceid'],FALSE);
					
				   
					
					foreach ($parameters as $parameter)
					{
						if($parameter["name"]=="default_EST")
							$default_EST=$parameter["value"];
						elseif($parameter["name"]=="default_LST")
							$default_LST=$parameter["value"];
						elseif($parameter["name"]=="default_max_delay")
							$default_max_delay=$parameter["value"];
						elseif($parameter["name"]=="default_mode")
							$default_mode=$parameter["value"];
							
						}
						
					//fi default are set schedule load    
					$load=Array();  
					$response["result"]="failure";
					if($default_EST=="" and $default_LST=="" and $default_max_delay!="") 
					{
						$now = new DateTime();
						$load['EST']=date('Y-m-d H:i',$now->getTimestamp());
                       
                     //20 secs to allow taskscheduler for receiving the  tasks with now < LST 
                     if ($default_max_delay < 20)
                          $default_max_delay = 20;
                        
						$now->add(new DateInterval('PT'.$default_max_delay.'S')); // adds   secs
						
						$load['LST']=date('Y-m-d H:i',$now->getTimestamp());;
						$load['deviceID']=$automata["deviceid"];
						$load['execution_type']='single_run'; 
						$load['mode']=substr($default_mode,0,strpos($default_mode,":"));
						$load['profile']=$automata["filename"];
						$load["modeName"]=substr($default_mode,strpos($default_mode,":")+1);
					   // var_dump($load);
						$response = $this->add($automata["userid"],json_encode($load));
					   // var_dump($response);
						
						}
					elseif($default_EST!="" and $default_LST!="" and $default_max_delay=="")
					{ 
						// It needs to define the date (today or tomorrow
						$now = new DateTime();
						$temp = date('m/d/Y',$now->getTimestamp());
						$lst = new DateTime($temp." ".$default_LST);
						$lst = $lst->getTimestamp();
						$est = new DateTime($temp." ".$default_EST);
						$est = $est->getTimestamp();
						$now = $now->getTimestamp();
						
						if($lst>$now)
							{
								$load['LST']=date('Y-m-d H:i',$lst);
								if($est>$now)
									$load['EST']=date('Y-m-d H:i',$est);
								else
									$load['EST']=date('Y-m-d H:i',$now);

							}
						 else
						 {
							  
							  $est=$est+86400;
							  $load['EST']=date('Y-m-d H:i',$est);
							  $lst=$lst+86400;
							  $load['LST']=date('Y-m-d H:i',$lst);
							 }
						
						
						$load['deviceID']=$automata["deviceid"];
						$load['execution_type']='single_run';
						$load['mode']=substr($default_mode,0,strpos($default_mode,":"));
						$load['profile']=$automata["filename"];
						$load["modeName"]=substr($default_mode,strpos($default_mode,":")+1);
						$response=$this->add($automata["userid"],json_encode($load));
					
						}
						if($response["result"]=="success")
						{
                        $this->submitLoad($response["taskID"]);
                       
						}
						
					return $response;
						}
					}
					
    public function submitLoad($loadid)
    {
		
		  $task=$this->getTask(1, $loadid);
         
		  $jsonstring=json_encode($task);
		  $jsonstring=strtr ($jsonstring, array ('"' => "'"));
		  $url='http://localhost:8808/add.json?taskid='.$loadid.'&jsons="'.urlencode($jsonstring).'"';
		  $this->log->info($url);
		  $this->restRequest($url);
		  $this->update($loadid,"{'status':0,'submittime':".time()."}");
      }        
            
	 public function endLoad($userid,$loadid)
	 {
		 
        $task = $this->getTask($userid,$loadid);
        
        $energy = 0;
        if($task['result']=='success')
        {
            $task = $task['task'];
            if($task['status']>=5) return 0;
            $query ="SELECT id from feeds WHERE name='device".$task['deviceID']."_in_kwh'";
            $queryres = $this->mysqli->query($query);
             $now = new DateTime();
            if($queryres->num_rows > 0)
            {
                $obj = $queryres->fetch_object();
                
                $last_value=$this->feed->get_value($obj->id);
                
                $ast = strtotime($task['AST']);
               
                        
                $logs=$this->feed->get_data($obj->id,$ast*1000,$now->getTimestamp()*1000,2);
                if (count($logs)<2)
                    $energy=0;
                else
                    $energy = $logs[1][1]-$logs[0][1];
                
            }
            
              if(array_key_exists('vendor',$task) and $task['vendor']>0)
                        $this->update_neighborhood2household($userid,$energy);
             $this->mysqli->query("UPDATE  user_tasks set status=5,ltime=".$now->getTimestamp()." where  id=$loadid");
             $this->mysqli->query("UPDATE  user_task_par set value=5 where name='status' and taskid=$loadid");
             $url='http://localhost:8808/del.json?taskid='.$loadid.'&energy='.$energy;
             $this->log->info($url);
             $this->restRequest($url);
             
         
             return 1;
             
        }
		 
		 }  
		 
	public function neighborhood_status($status)
		{
			if( in_array($status, array("connected","disconnected")))
			 $url='http://localhost:8808/neighborood.json?status='.$status;
			else
			 $url='http://localhost:8808/neighborood.json';
			
			$response = json_decode($this->restRequest($url),true);
			return $response["status"];
			
			
			
			}
			
			
	public function neighborhood_list()
		{
			 $url='http://localhost:8808/neighbors.json';
			 $response = json_decode($this->restRequest($url),true);
			 return $response["result"];

			}
	public function neighborhood_remove($jid)
		{
			 $url='http://localhost:8808/delneighbor.json?jid='.$jid;
			 $response = json_decode($this->restRequest($url),true);
			 return $response["result"];

			}
			
    	public function  startDevice( $userid,$deviceid)
        {
            return $this->devices->set_device_status($userid,$deviceid,1);
            }
    
       public function  stopDevice( $userid,$deviceid)
        {
            
            return $this->devices->set_device_status($userid,$deviceid,0);
            }
           


      /*Learning Functions */
            
            
      /* This function initialize the learning automata and read/save their status in learning
       * It calls specific functions to evolve according to the operating Type 
       * */
      public function updateLearning($id,$time,$value)
      {
          if($this->redis)
          {
			 //error_log("redis");
			 if (!$this->redis->exists("profiles:$id"))
			 {
				  //error_log("no profile");
                $input=$this->input->getInput($id);
                $automata = Array();
                $automata["id"] =$id;
				  $automata["status"]=0;
				  $automata["userid"]=$input['userid'];
                $automata["end"]=$time;
                $automata["nodeid"]=$input['nodeid'];
                $automata = $this->loadTemplate($automata);
                
                //adding parameters of specific operatingType
                if($automata["operatingType"]=="continuously_run")
                    {
                        $automata["off_start"]=0;
                        $automata["off_end"]=0;
                        $automata["off_duration"]=0;
                        $automata["on_start"]=0;
                        $automata["on_end"]=0;
                        $automata["on_duration"]=0;
                        $automata["accumulated_delay"]=0;
                        $automata["loadid"]=-1;
                   
                    }
                elseif($automata["operatingType"]=="continuously_run")
                {
                    $automata["on_end"]=0;
                    $automata["on_start"]=0;
                    }
          
          }
            else 
                $automata = $this->redis->hGetAll("profiles:$id");
 

           
            $automata["power"]=$value;
            $power = $value;
            $automata["time"]=$time;
            
           
            if($automata["operatingType"]=="continuously_run")
                $automata = $this->updatePeriodicLearning($automata, $time,$value);
            elseif($automata["operatingType"]=="single_run")
                $automata = $this->updateSingleRunLearning($automata, $time,$value);
                
                
            #save the status of the automata    
            $this->redis->hMSet("profiles:$id",$automata);	
            
          }//redis works
          return $automata;
        }
		


public function updateSingleRunLearning($automata, $time,$value)
    {
        
        $parameters=$this->devices->get_parameters($automata['deviceid'],FALSE);
        foreach ($parameters as $parameter)
               if($parameter["name"]=="noise")
                   $automata["noise"]=intval($parameter["value"]);
               elseif($parameter["name"]=="silence")
                   $automata["silence"]=intval($parameter["value"]);
               elseif($parameter["name"]=="silence_start")
                    $automata['silence_start']=intval($parameter["value"]);
              
                $pars = Array();
                if (!array_key_exists("silence_start",$automata)) 
                    {
                        $pars["silence_start"]=0;
                        $automata["silence_start"]=0;
                    }
                if(sizeof($pars)>0) $this->updateSQLParameters($pars,$automata['deviceid']);
        
        $power = $value;
        if($power>$automata["noise"])   
        {
          
          if($automata["status"]==0)
              {  
                    if(!array_key_exists("on_start",$automata))
                        $automata['on_start']=0;
                  
                    $startok = False;
                    if($automata['silence_start']==0)
                       { $startok = True;
                         $automata['on_start']=$time;
                       }
                    elseif($automata['on_start']==0)
                        $automata['on_start']=$time;
                    elseif($time-$automata['on_start']<=$automata['silence_start'])
                        $startok=True;
                    else
                        $automata['on_start']=$time;
                    
                    if($startok)
                    {    
                        //It seems that the device has  started right now
                        //We remember that the device is running
                        $automata["status"]=1;     
                        //check if it needs to automatically handle the device
                        $response = $this->auto($automata);
                        if($response["result"]=="success") 
                            {
                            
                            $automata["loadid"]=$response["taskID"];
                            
                            $task = $this->getTask($automata['userid'],$automata['loadid']);
                            ////////////////////////////////////////
                            error_log("[MAS] checking if AST>EST".strtotime($task['task']["AST"])." ".strtotime($task['task']["EST"]));
                            if (strtotime($task['task']["AST"])>strtotime($task['task']["EST"]))
                              {
                                $this->log->warn("[MAS] AST>EST ".strtotime($task['task']["AST"])." ".strtotime($task['task']["EST"]));
                                $this->stopDevice($automata["userid"],$automata["deviceid"]);
                                $automata["status"]=2;   
                              }
                              else $automata["status"]=1;  
                            
                           
  
                            }
                         else 
                             {
                                 $automata["status"]=1;  
                             }
                    }
                    
              }
            else if($automata["status"]==2)
                         {
                            
    
                            $task = $this->getTask($automata['userid'],$automata['loadid']);
                            if($task != NULL)
                            {
                                $this->log->warn("[MAS] device ".$automata["deviceid"]."has been switched on before AST".$task['task']["AST"]);
                                $this->update( $automata["loadid"],"{'status': 4}");
                            } 
                            $automata["status"]=1;  
                            $automata['on_start']=$time;
                         }
 
              $automata["on_end"]=$time;
                            
                           
             }
                    //the device is not consuming but switched on
         elseif($automata["status"]==1 and  $power<=$automata["noise"] and ($time-$automata["on_end"])>$automata["silence"])
         
            {
                error_log("[STOPDEBUG] power:".$power." time:".$time." lasttimeovernoise:".$automata["end"]);
				 //the load is completed   
               $automata["status"]=0;
    			 if( array_key_exists("loadid",$automata)  and  $automata["loadid"]>0)
                {
                    $current_task_status = $this->getTaskStatus($automata["loadid"]);   
                    $AET = date('Y-m-d H:i',$automata["on_end"]); 
                    if($current_task_status ==4)
                     {
                        
                        
                        
                            //load status is updated here!! Notify TaskManager and avoid Load Controller?
                        $this->update( $automata["loadid"],"{'status': 5,'AET':'".$AET."'}");
                        $task = $this->getTask($automata['userid'],$automata['loadid']);
                        
                        #$this->updateSingleRunProfile($automata['userid'], $automata['deviceid'], $task['task'] ['mode']);
                        
                        $feedid = $this->feed->get_id($automata['userid'], 'device'.$automata['deviceid'].'_in_kwh');      
                        $this->rawProfileToFile($feedid,$automata['on_start'], $automata['on_end'],180,$automata["filename"]);  
                        
                        $automata["status"]=0;
                        $automata['on_start']=0;
                        }
                        
                     else if($current_task_status <4 )
                         $this->update( $automata["loadid"],"{'status': 6,'AET':'".$AET."'}");
                     
                    }
             }
             //the load is wating for AST
             elseif($automata["status"]==2)    
                      {
                         $task = $this->getTask($automata['userid'],$automata['loadid']);
                         if($task["result"]=="success")
                         {
                           if(time()>strtotime($task['task']["AST"]))
                            { 
                    
                                $this->startDevice( $automata["userid"],$automata["deviceid"]);
                                $this->update( $automata["loadid"],"{'status': 4}");
                                $automata["status"]=1;
                                $automata["on_start"]=time();
                                $automata["on_end"]=time();
                             }
                         }
                         else{
                             
                             error_log("[MasModule] Error task not found");
                             
                             }

                    }
        
        
         return $automata;
        
        }
        
        

public function updatePeriodicLearning($automata, $time,$value){
        #we suppose that the controller of the devices, after the smart pluf is switched on,
        # takse no more tha $offset seconds to start the device if necessary
        
        $offset = 30;
        $parameters=$this->devices->get_parameters($automata['deviceid'],FALSE);
        foreach ($parameters as $parameter)
               if($parameter["name"]=="noise")
                   $automata["noise"]=intval($parameter["value"]);
               elseif($parameter["name"]=="allowed_delay")
                   $automata["allowed_delay"]=intval($parameter["value"]);
               elseif($parameter["name"]=="silence")
                   $automata["silence"]=intval($parameter["value"]);
       
        $pars = Array();
        if (!array_key_exists("allowed_delay",$automata)) $pars["allowed_delay"]=0;
        if(sizeof($pars)>0) $this->updateSQLParameters($pars,$automata['deviceid']);
       
       
        #device has   started
        if ($automata['status']==0 and $value > $automata['noise'])
            { 
               $automata['status']=1;
               
               if (floatval($automata['off_end']) > 0)
                  {
                      error_log("updating duration");
                      $automata['off_duration']=$time-($time-$automata['off_end'])/2-$automata['off_start'];
                  }

              
                if (array_key_exists('loadid',$automata))
                {
                    $task = $this->getTask($automata['userid'],$automata['loadid']);
     
                    #the start happened  at AST
                    if( array_key_exists('AST',$task)   and ($time < ($task['AST']+$offset)))
                        $automata['accumulated_delay']=$automata['accumulated_delay']+$task['AST']-$task['EST'];

                }
                
                if($automata['loadid']>0)
                  {
                        $this->update( $automata["loadid"],"{'status': 4}");
                        
                  }
                    
                $automata['off_end']=$time;
                $automata['on_start']=$time;
                $automata['on_end']=$time;
            }
         #the device is still running   
         else if($automata['status']==1 and $value > $automata['noise'])

            $automata['on_end']=$time;

         #the device is stopped
         else if($automata['status']==1 and floatval($value)<=floatval($automata['noise']) and ($time - floatval($automata['on_end']))> $automata["silence"])
         {

           #in this case a load is just terminated
           $automata['status'] = 0;
           #update the positive profile
           if ($automata['on_end'] > 0)
               $automata['on_duration'] = $time - ($time - floatval($automata['on_end'])) / 2 - floatval($automata['on_start']);


           # update the total start time
           $automata['off_start']=$time;
           $automata['off_end']=$time;
           $automata['on_end']=$time;

           $feedid = $this->feed->get_id($automata['userid'], 'device'.$automata['deviceid'].'_in_kwh'); 
               
           $this->rawProfileToFile($feedid,$automata['on_start'], $automata['on_end'],180,$automata["filename"]);
           
           //$this->updateProfile($automata['userid'],$automata['deviceid'],$automata['modeid'],1);

           if (floatval($automata['accumulated_delay']) < floatval($automata['allowed_delay']) and  floatval($automata['off_duration']) > 0)
            {
               #schedule next load
               $EST = intval($time + $automata['on_duration']);
               $delay= max( 0, $automata['allowed_delay']-$automata['accumulated_delay']);
               $LST = intval($EST + $delay);
                
             
                   
                 
                 
                
                if($automata['loadid']>0)
                  {
                        $this->update( $automata["loadid"],"{'status': 5}");
                        $automata["loadid"]=-1;
                  }
                
                
                if ($delay > 0){
                 
                    $dt = new DateTime("@$EST");
                    $EST=$dt->format('Y-m-d H:i');
                    $dt = new DateTime("@$LST");
                    $LST=$dt->format('Y-m-d H:i');
       
                    $jsonString = "{'EST': '".$EST."','LST': '".$LST."','deviceID': '".$automata["deviceid"]."','execution_type':'periodic_run', 'modename': '".$automata['modeName']."','mode' : '".$automata["mode"]."', 'profile': '".$automata["filename"]."'}";		 
                            
                    $response = $this->add($automata['userid'], $jsonString);
                    if($response["result"]=="success") 
                    {
                        #switch oof and submit Load
                      $this->stopDevice($automata["userid"],$automata["deviceid"]);
                      $automata["loadid"]=$response["taskID"];
                      $automata["status"] = 2;
                      $this->submitLoad($response["taskID"]);
                      $this->update( $automata["loadid"],"{'status': 0}");
                        }
                }
            }
         } elseif($automata['status']==2 and $value<=$automata['noise'])
         {   
                $task = $this->getTask($automata['userid'],$automata['loadid']);
                
                if(time()>strtotime($task['task']["AST"]))
                { 
                    $this->startDevice( $automata["userid"],$automata["deviceid"]);
                    $automata["status"]=0;
                    $automata["on_end"]=$time;
                    $automata["on_start"]=$time;
                }
                
             
             }


        return $automata;
    
}


public function loadTemplate($loadTemplate){
			
                $deviceid=$this->devices->get_device_by_nodeid($loadTemplate["userid"], $loadTemplate["nodeid"]);
                $device=$this->devices->get_device_by_deviceid($loadTemplate["userid"], $deviceid['deviceid']);
                $parameters=$this->devices->get_parameters($deviceid['deviceid'],FALSE);
                
                 
                
                foreach ($parameters as $parameter)
                                 if($parameter["name"]=="noise")
                                        $loadTemplate["noise"]=intval($parameter["value"]);
                                 elseif($parameter["name"]=="allowed_delay")
                                        $loadTemplate["allowed_delay"]=intval($parameter["value"]);
                                 elseif($parameter["name"]=="silence")
                                        $loadTemplate["silence"]=intval($parameter["value"]);
                                 elseif($parameter["name"]=="silence_start")
                                        $loadTemplate["silence_start"]=intval($parameter["value"]);
               
 
                
 
 
                
                $pars = Array();
                if (!array_key_exists("noise",$loadTemplate)) $pars["noise"]=0; 
                if (!array_key_exists("silence",$loadTemplate)) $pars["silence"]=0;
                if (!array_key_exists("silence_start",$loadTemplate)) $pars["silence_start"]=0;
                
                if(sizeof($pars)>0) 
                    { $this->updateSQLParameters($pars,$deviceid['deviceid']);
                      $loadTemplate = $loadTemplate + $pars;
                      error_log("loadtemplate");
                      error_log("".var_dump($loadTemplate));
                    }
                    

                 
 
                $template=$this->devices->get_template($loadTemplate["userid"], $device['templateid']);
                $loadTemplate["operatingType"]=$template->operatingType;
                
                //error_log("".var_dump($template->modes));
                $loadTemplate["modes"]="[";
                
                foreach ($template->modes as $mode)
                   $loadTemplate["modes"]= $loadTemplate["modes"]."'".$mode."', ";
                $loadTemplate["modes"] =substr($loadTemplate["modes"],0,strlen($loadTemplate["modes"])-2)."]";
                   
                $default_mode = $template->modes[0];
                $loadTemplate["mode"]=substr($default_mode,0,strpos($default_mode,":"));
                $loadTemplate["deviceid"]=$deviceid['deviceid'];
                $loadTemplate["available"]=0;
                $loadTemplate["filename"] = "u".$loadTemplate["userid"]."d".$loadTemplate["deviceid"]."m".$loadTemplate["mode"].".prof";
                $loadTemplate["modeName"] =$template->modes[0];
                $feedid = $this->feed->get_id($loadTemplate['userid'], 'device'.$loadTemplate['deviceid'].'_in_kwh');      
                $loadTemplate["feedid"] = $feedid;
                
                
        
                $query = "select id from user_tasks where  status<5  and deviceid=".$loadTemplate['deviceid'];
                $qresult = $this->mysqli->query($query);
                if ($qresult->num_rows > 0)
                 {  
                     $row = $qresult->fetch_object();
                     $loadTemplate["taskID"] = $row->id; 
                 }
                        
                        return $loadTemplate;
                }


        /*****Profile Management******/
        
public function rawProfileToFile($feedid, $start,$end,$delta,$filename)
{
    
        $dp = ($start-$end)/$delta;
        if ($dp<10)    $dp=10;
        
        $ts = $this->feed->get_data($feedid,$start*1000,$end*1000,$dp);
        
        $startime=-1;
        $totalenergy =0;
        // inserisce i valori ricevuti dal form in coda al file
        if(isset($ts))
        {   
            if (count($ts)>2)
                {
                        $fp = fopen('/var/www/emoncms/profiles/'.$filename,"w");
                    
                        foreach($ts as $sample) 
                        {
                              if($startime==-1)
                                  { $startime=$sample[0];
                                    $startenergy=$sample[1];
                                  }
                              $time = ($sample[0]-$startime)/1000;
                              $energy =$sample[1]-$startenergy;
                              fputs($fp, $time." ".$energy."\n");
                              $totalenergy = $totalenergy + $energy;
                                
                          }
                          
                           // chiude il file
                            fclose($fp);
                }
                
            }
        
       
        
        $result = array();
        $result["success"]=true;
        $result["filename"]=$filename;
        return $result;
    }        

	 public function updateSingleRunProfile($userid, $deviceid, $modeid)
    {
		$time_threshold = 10*60000;
		
		$result = Array();
		$ast = Array();
		$endtime = Array();
		$qresult = $this->mysqli->query("select id, ast,ltime from  user_tasks  where deviceid=".$deviceid." and modeid=$modeid and ast <> 0 and status=5 order by ast desc limit 5");
		 $ast[0]= time();
		if ($qresult->num_rows > 0)
		 {
			 
			 $i=0;
			 $feedid = $this->feed->get_id($userid, 'device'.$deviceid.'_in_kwh'); 
			 $profile = Array();
			 $duration =0;
			
			 $mpath="/var/www/emoncms/profiles";
			 $filename="u".$userid."d".$deviceid."m".$modeid;
			 $fp = fopen($mpath."/".$filename.".raw", 'w');

		 
			 while($row = $qresult->fetch_object())
			    {	
					
					
					  
					  $i= $i + 1;
					  $ast[$i] = $row->ast;
					  $temp =  $row->ltime;
					  if($temp==0)
					    $endtime[$i]=$ast[$i-1]*1000;
					  else
					    $endtime[$i]=$temp*1000;
					  
					  //echo "begin:".$ast[$i]." ".$endtime[$i]."\n";
					  
					  $start=$ast[$i]*1000;
					  $data = $this->feed->get_data_int($feedid,$start,$endtime[$i],5);
                    
					  $ast[$i]=$data[0][0]/1000;
					  $offset=$data[0][0];
					  $eoffset = $data[0][1];
					  //echo var_dump($data);
					  $previous=0;
					  $j=0;
					  $endtime[$i]=$data[0][0];
					  //echo count($data)."\n";
					  
					  while($data[$j][1]==0 and $j<count($data))  
						$j=$j+1;
							 
							 
					 
					  while($j<count($data))
					   {
						 $incr = $data[$j][1]-$previous;
						 //echo "incr:".$incr."\n";
						 //if there is an increment the task is not finished
						 
						 if( ($data[$j][0]-$endtime[$i])< $time_threshold) // sample is not too far
						 {
							if($temp==0 and $incr > 0 and ($data[$j][0]-$endtime[$i])<$time_threshold)
								$endtime[$i]=$data[$j][0];
					
							
						    
							fputcsv($fp, [ceil($data[$j][0]-$offset)/1000, ($data[$j][1]-$eoffset)]);
							//echo $data[$j][0]." ".$data[$j][1]."\n";
							//the valued has  not changed for too much time
							if($temp == 0 and  $incr==0 and ($data[$j][0]-$endtime[$i])>$time_threshold)
								{	$silence = $data[$j][0]-$endtime[$i];
									//echo "AFTER: start:".$ast[$i]."end:".$endtime[$i]."silence:".$silence."\n";
									$j = count($data);
								
								}
							else 
								{
									$previous= $data[$j][1];
									
								}
							
							}// sample is not too far
							
							$j=$j+1;
					    }//end whle
						
					    if($temp==0 and ($endtime[$i]-$ast[$i]*1000)> 0)
					      {
							   $this->mysqli->query("update user_tasks set ltime=".($endtime[$i]/1000)." where id=".$row->id);
							   if($duration < ($endtime[$i]-$ast[$i]*1000))
								$duration =$endtime[$i]-$ast[$i]*1000;
						  }
					    
					    
				  }
						
						fclose($fp);
						shell_exec("python /var/www/emoncms/Modules/mas/bin/updateprof.py ".$filename);
						$result["success"]=true;
						$result["profile"]=$filename.".prof";
			      
		}    
		else
		{
			$result["success"]=false;
			$result["message"]="profile not available";
			}
		 
		 return $result;
		}	 

public function updateProfile($userid, $deviceid,$modeid,$raw)
{
    
    
    $device=$this->devices->get_device_by_deviceid($userid, $deviceid);
    if(! $device) return "false";
    $template=$this->devices->get_template($userid, $device['templateid']);

    if($template->operatingType=="single_run")
        { 
            $qresult = $this->mysqli->query("select id, ast,ltime from  user_tasks  where deviceid=".$deviceid." and modeid=$modeid and ast <> 0 and status=5 order by ast desc limit 5");
            if ($qresult->num_rows > 0)
            {
             if($raw==1)
             {
                    $row = $qresult->fetch_object();
                    $start = $row->ast;
                    $end = $row->ltime;
                    
                    $feedid = $this->feed->get_id($userid, 'device'.$deviceid.'_in_kwh'); 
                    return $this->rawProfileToFile($feedid,$start, $end,120,"u".$userid."d".$deviceid."m".$modeid.".prof_raw");  
                 }
              else
                   {
                    return $this->updateSingleRunProfile($userid, $deviceid, $modeid);
                    
                   }
            }
            else return "no loads available";
            
            }
    else if($template->operatingType=="continuously_run"){
        ;
        
        }
    
    }
    
    
function updateSQLParameters($parameters,$deviceid)
{
    $keys = array_keys($parameters);
    foreach ($keys as $key)
    {
        $query = "INSERT INTO user_device_par VALUES (NULL,".$deviceid.",'".$key."','".$parameters[$key]."' )";
        $this->mysqli->query($query);
        }
    
    }
    
}
